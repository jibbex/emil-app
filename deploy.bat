rename "..\emil-server\target\emil-server-0.0.2-runner" "emil-server-0.0.2"
ssh mm@michm.de "kill $(ps -ef | grep emil-server-0.0.2 | awk '{print $2}')"
scp "..\emil-server\target\emil-server-0.0.2" mm@michm.de:/home/mm/emil-server-0.0.2
ssh mm@michm.de "chmod +x emil-server-0.0.2"
ssh mm@michm.de ./start-emil.sh
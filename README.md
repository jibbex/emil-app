# E.M.I.L. App

Extended Mathematical Improvement and Learning is an app to improve your math skills.

**A preview web build is available at: [emil.michm.de](https://emil.michm.de).**

![](https://git.michm.de/emil/emil-app/-/raw/master/assets/app.mp4)

> The multiplayer mode is currently in active development and works only rudimentary.

---

## Building

### Web

<mark>Rendering with HTML causes issues. I recommend to use canvaskit for rendering. </mark>

Run following command to build for web:

```
flutter build web --pwa-strategy=none --release --web-renderer canvaskit
```

### Android

Run following command to build for Android:

```
flutter build apk --release
```

### Windows

Run following command to build for Windows:

```
flutter build windows --release
```

import 'dart:io';
import 'package:emil_app/src/l10n/emil_app_localizations_delegate.dart';
import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/models/high_scores/high_scores.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/views/calculation_view.dart';
import 'package:emil_app/src/views/configuration_view.dart';
import 'package:emil_app/src/views/high_scores_view.dart';
import 'package:emil_app/src/views/join_view.dart';
import 'package:emil_app/src/views/multiplayer_view.dart';
import 'package:emil_app/src/views/qr_scan_view.dart';
import 'package:emil_app/src/views/start_view.dart';
import 'package:emil_app/src/widgets/app_bar.dart';
import 'package:emil_app/src/widgets/app_bar_button.dart';
import 'package:emil_app/src/widgets/custom_error.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

/// Entry point of the application. Initializes localization and inflates
/// the root widget to the screen. The initialized localization is passed
/// to EmilApp widget.
void main() async {
  final local = await EmilAppLocalizationsDelegate().load(const Locale('de'));
  runApp(EmilApp(localizations: local));
}

/// Root widget of emil app. An MultiProvider widget with two
/// ChangeNotifierProvider for configuration and high scores wraps
/// a MaterialApp widget. The class provides for each view an builder
/// method, which are mapped to the named routes.
class EmilApp extends StatelessWidget {
  final EmilAppLocalizations localizations;
  static const String kProductionUrl = 'wss://emil.michm.de/socket';
  static const String kDebugUrl = 'wss://emil.michm.de/socket';

  // static const String kDebugUrl = 'ws://localhost:12544/socket';

  const EmilApp({
    super.key,
    required this.localizations,
  });

  @override
  Widget build(BuildContext context) {
    /// Initialization of app theme.
    ThemeData appTheme = theme(context);
    return MultiProvider(
      providers: [
        /// Configuration class and is through the ChangeNotifierProvider
        /// in the whole application context available.
        ChangeNotifierProvider<Configuration>(
          create: (context) {
            Configuration conf = Configuration(localizations);
            conf.load();
            return conf;
          },
        ),

        /// The state above is also true for HighScores class. This is maybe
        /// more than needed at this point...
        ChangeNotifierProvider<HighScores>(
          create: (context) {
            HighScores scores = HighScores();
            scores.loadData();
            return scores;
          },
        ),
      ],
      builder: (context, child) {
        appTheme = theme(context, true);
        return child!;
      },

      /// In the MaterialApp widget, 4 properties are particularly important
      /// and/or were hard to find in Flutter's documentation.
      ///
      /// The color property sets the theme-color property for web build.
      ///
      /// localizationsDelegates list includes an EmilAppLocalizationDelegate
      /// instance.
      ///
      /// The builder property expects a builder method, which is used to
      /// insert widgets above the Navigator. You can overwrite the static
      /// builder method of the globally ErrorWidget and change the default
      /// appearance of error messages.
      ///
      /// The theme property should be assigned an instance of your theme.
      child: MaterialApp(
        color: kAltPrimary,
        title: "E.M.I.L.",
        onGenerateTitle: (context) =>
            EmilAppLocalizations.of(context)!.appTitle,
        localizationsDelegates: [
          EmilAppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        builder: (context, child) {
          ErrorWidget.builder = (errorDetails) {
            return CustomError(
              context: context,
              errorDetails: errorDetails,
            );
          };

          return child!;
        },
        theme: appTheme,
        supportedLocales: const [
          Locale('en', ''),
          Locale('de', ''),
        ],
        onUnknownRoute: (settings) {
          if (settings.name != null && settings.name!.startsWith('/channel')) {
            final paths = settings.name!.split('/');
            final channel = paths[paths.length - 1];

            return MaterialPageRoute(
              builder: (context) => JoinView(
                baseUrl: kProductionUrl,
                channel: channel,
                appBar: appBar(
                  context: context,
                  actions: [
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: AppBarButton(
                        const Icon(Icons.settings),
                        onPressed: () =>
                            Navigator.of(context).pushNamed('/settings'),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ),
                  ],
                  title: localizations.multiplayer,
                ),
              ),
            );
          } else {
            return MaterialPageRoute(
              builder: _startViewBuilder,
            );
          }
        },
        routes: {
          '/': _startViewBuilder,
          '/play': _calculationViewBuilder,
          '/settings': _configurationViewBuilder,
          '/highscores': _highScoresViewBuilder,
          '/multiplayer': _multiplayerViewBuilder,
          '/scanning': _qrScanViewBuilder,
        },
      ),
    );
  }

  /// Navigates back to the start view regardless of
  /// the number of previous views.
  void _popToStart(BuildContext context) {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: _startViewBuilder,
      ),
    );
  }

  /// StartView widget builder method. It handles the building
  /// of the Start View.
  Widget _startViewBuilder(BuildContext context) {
    EmilAppLocalizations localizations = EmilAppLocalizations.of(context)!;
    double width = MediaQuery.of(context).size.width;

    return StartView(
      appBar: appBar(
        context: context,
        actions: [
          Padding(
            padding: const EdgeInsets.all(5),
            child: AppBarButton(
              const Icon(Icons.settings),
              onPressed: () => Navigator.of(context).pushNamed('/settings'),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
        ],
        title:
            width > 960 ? localizations.appTitle : localizations.appShortTitle,
      ),
    );
  }

  /// CalcView widget builder method. It handles the building
  /// of the Calculation View.
  Widget _calculationViewBuilder(BuildContext context) {
    EmilAppLocalizations localizations = EmilAppLocalizations.of(context)!;
    double width = MediaQuery.of(context).size.width;

    return CalculationView(
      appBar: appBar(
        context: context,
        title:
            width > 960 ? localizations.appTitle : localizations.appShortTitle,
        leading: AppBarButton(
          const Icon(Icons.arrow_back_ios_new),
          onPressed: () => Navigator.of(context).pop(),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
      ),
    );
  }

  /// ConfigurationView widget builder method. It handles the building
  /// of the Configuration View.
  Widget _configurationViewBuilder(BuildContext context) {
    String title = EmilAppLocalizations.of(context)!.settingsTitle;
    context.read<Configuration>().load();

    return ConfigurationView(
      appBar: appBar(
        context: context,
        title: title,
        leading: AppBarButton(
          const Icon(Icons.arrow_back_ios_new),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          onPressed: () => _popToStart(context),
        ),
      ),
    );
  }

  /// HighScoresView widget builder method. It handles the building
  /// of the High Scores View.
  Widget _highScoresViewBuilder(BuildContext context) {
    String title = EmilAppLocalizations.of(context)!.hallOfFame;

    return HighScoresView(
      appBar: appBar(
        context: context,
        title: title,
        leading: AppBarButton(
          const Icon(Icons.arrow_back_ios_new),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          onPressed: () => _popToStart(context),
        ),
      ),
    );
  }

  /// MultiplayerView widget builder method. It handles the building
  /// of the Multiplayer View.
  Widget _multiplayerViewBuilder(BuildContext context) {
    String title = EmilAppLocalizations.of(context)!.multiplayer;

    return MultiplayerView(
      baseUrl: kDebugMode ? kDebugUrl : kProductionUrl,
      appBar: appBar(
        context: context,
        title: title,
        leading: AppBarButton(
          const Icon(Icons.arrow_back_ios_new),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          onPressed: () => _popToStart(context),
        ),
        actions: [
          if (kIsWeb || !Platform.isWindows && !Platform.isLinux)
            Padding(
              padding: const EdgeInsets.all(5),
              child: AppBarButton(
                const Icon(Icons.qr_code_scanner_rounded),
                onPressed: () => Navigator.of(context).pushNamed('/scanning'),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            )
        ],
      ),
    );
  }

  /// QrScanView widget builder method. It handles the building
  /// of the QR Scan View.
  Widget _qrScanViewBuilder(BuildContext context) {
    String title = EmilAppLocalizations.of(context)!.scanning;

    return QrScanView(
      baseUrl: kDebugMode ? kDebugUrl : kProductionUrl,
      appBar: appBar(
        context: context,
        title: title,
        leading: AppBarButton(
          const Icon(Icons.arrow_back_ios_new),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
    );
  }
}

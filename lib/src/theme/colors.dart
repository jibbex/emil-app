import 'package:flutter/material.dart';

const Color kBackground = Color(0xffef5460);
const Color kLightBackground = Color(0xffd5d0d0);
const Color kPrimaryIcon = Color(0xffd5d0d0);
const Color kPrimaryIconAlpha = Color(0xccd5d0d0);
const Color kPrimary = Color(0xffdf1d5c);
const Color kAltPrimary = Color(0xffe5605b);
const Color kOverlay = Color(0xa6b01530);
const Color kOnPrimary = Color(0xfff69191);
const Color kSecondary = Color(0xffe08768);
const Color kError = Color(0xffa93245);
const Color kHint = Color(0xffa93245);
const Color kGmBtnTextColor = Color(0x73ffffff);
const Color kSecondaryText = Color(0xaaeeeeee);
const Color kPrimaryText = Color(0xff570f28);
const Color kShadow = Color(0x267c1132);

Gradient kBackgroundGradient = const LinearGradient(
  transform: GradientRotation(0.7),
  stops: [ 0.0, 1.0],
  colors: <Color>[
    kAltPrimary,
    kPrimary,
  ],
);

Gradient kBackgroundGradient2 = const LinearGradient(
  transform: GradientRotation(0.7),
  stops: [ 0.0, 1.0],
  colors: <Color>[
    kOnPrimary,
    kLightBackground,
  ],
);

Gradient backgroundGradientWithOpacity(double opacity) => LinearGradient(
  transform: const GradientRotation(0.7),
  stops: const [ 0.0, 1.0],
  colors: <Color>[
    const Color(0xffe5605b).withOpacity(opacity),
    kPrimary.withOpacity(opacity),
  ],
);

Gradient backgroundGradient2tWithOpacity(double opacity, double opacity2) => LinearGradient(
  transform: const GradientRotation(0.7),
  stops: const [ 0.0, 1.0],
  colors: <Color>[
    kLightBackground.withOpacity(opacity),
    kSecondary.withOpacity(opacity2),
  ],
);

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'colors.dart';

const double kGmBtnFontSize = 40;

const Set<MaterialState> kInteractiveStates = <MaterialState>{
  MaterialState.pressed,
  MaterialState.hovered,
  MaterialState.focused,
};

final MaterialStateProperty<Color?> btnBackground =
    MaterialStateProperty.resolveWith((Set<MaterialState> states) =>
        states.any(kInteractiveStates.contains) ? kAltPrimary : kPrimary);

TextStyle gmBtnTextStyle = TextStyle(
  fontSize: kGmBtnFontSize,
  color: kGmBtnTextColor.withOpacity(0.3),
  fontFamily: 'Condensed',
  fontWeight: FontWeight.w200,
);

EdgeInsets getContainerSpacing(BuildContext context) {
  Size deviceSize = MediaQuery.of(context).size;
  double width =
      deviceSize.width > 960 ? deviceSize.width * 0.1 : deviceSize.width * 0.05;

  return EdgeInsets.symmetric(
    vertical: 15,
    horizontal: deviceSize.width < 640 ? 20 : width,
  );
}

TextStyle eqTextStyle(BuildContext context) =>
    Theme.of(context).textTheme.headline2!.copyWith(
      color: Colors.white.withOpacity(0.4),
      leadingDistribution: TextLeadingDistribution.even,
      fontWeight: FontWeight.w100,
      decorationColor: kPrimary,
      fontSize: 16,
      shadows: [
        Shadow(
          color: Colors.black.withOpacity(0.25),
          offset: Offset.fromDirection(1.1, 2.75),
        )
      ],
    );

Widget getTitle(BuildContext context, String title, [TextStyle? style]) {
  final textStyle = Theme.of(context).textTheme.headline2!.copyWith(
    color: kGmBtnTextColor,
    leadingDistribution: TextLeadingDistribution.even,
    fontWeight: FontWeight.w200,
    decorationColor: kShadow,
    fontSize: style != null ? null : 24,
    shadows: [
      Shadow(
          color: Colors.black.withOpacity(0.3),
          offset: Offset.fromDirection(1.1, 0.75),
          blurRadius: 5),
      Shadow(
        color: Colors.black.withOpacity(0.2),
        offset: Offset.fromDirection(1.5, 2.1),
        blurRadius: 10,
      )
    ],
  );

  return Text(
    title,
    textAlign: TextAlign.center,
    style: textStyle.merge(style),
  );
}

ButtonStyle buttonStyle = ButtonStyle(
  elevation: MaterialStateProperty.resolveWith((Set<MaterialState> states) {
    if (states.any(kInteractiveStates.contains)) {
      return 4;
    }

    return 0;
  }),
  padding: MaterialStateProperty.resolveWith(
    (Set<MaterialState> states) => const EdgeInsets.all(25),
  ),
  backgroundColor:
      MaterialStateProperty.resolveWith((Set<MaterialState> states) {
    if (states.any(kInteractiveStates.contains)) {
      return kPrimaryIcon.withOpacity(0.1);
    }

    return kPrimary.withOpacity(0.25);
    // return Colors.transparent;
  }),
  shape: MaterialStateProperty.resolveWith(
    (Set<MaterialState> states) => const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(25)),
    ),
  ),
);

TextStyle headline1 = const TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w500,
  color: Color(0x66ffffff),
);

ThemeData _buildTheme(BuildContext context, bool isProviderSet) {
  const int kOverlayAlpha = 0x1f;
  final base = ThemeData.light();

  final titleTextStyle = base.textTheme.titleLarge?.copyWith(
    color: Colors.white60,
    fontSize: 20,
  );

  final iconTheme = base.iconTheme.copyWith(
    size: 24,
    color: Colors.white.withOpacity(.6),
  );

  final overlayState =
      MaterialStateProperty.resolveWith((Set<MaterialState> states) {
    if (states.any(kInteractiveStates.contains)) {
      return kOverlay.withOpacity(0.2);
    }

    return kPrimary.withOpacity(0.1);
    // return Colors.transparent;
  });

  return base.copyWith(
      useMaterial3: true,
      applyElevationOverlayColor: true,
      splashColor: kPrimaryText.withOpacity(0.1),
      hoverColor: kLightBackground.withOpacity(0.05),
      highlightColor: kPrimary.withOpacity(0.05),
      pageTransitionsTheme: const PageTransitionsTheme(
        builders: <TargetPlatform, PageTransitionsBuilder>{
          TargetPlatform.android: ZoomPageTransitionsBuilder(),
          TargetPlatform.windows: ZoomPageTransitionsBuilder(),
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        },
      ),
      dividerTheme: base.dividerTheme.copyWith(
        color: kShadow,
      ),
      colorScheme: base.colorScheme.copyWith(
        brightness: Brightness.dark,
        primary: kPrimary,
        onPrimary: kOnPrimary,
        secondary: kSecondary,
        error: kError,
        background: kBackground,
        outline: kShadow,
      ),
      brightness: Brightness.dark,
      primaryColor: kPrimary,
      primaryColorLight: kPrimary,
      primaryColorDark: kPrimary,
      canvasColor: kBackground,
      backgroundColor: kBackground,
      scaffoldBackgroundColor: kBackground,
      shadowColor: kShadow,
      sliderTheme: base.sliderTheme.copyWith(
          inactiveTrackColor: kShadow,
      ),
      cardColor: kLightBackground,
      cardTheme: base.cardTheme.copyWith(
        color: Colors.transparent,
        surfaceTintColor: Colors.transparent,
        shadowColor: Colors.transparent,
        margin: const EdgeInsets.all(0),
        elevation: 0,
      ),
      textTheme: base.textTheme.copyWith(
        bodyText1: base.textTheme.bodyText1?.copyWith(
          color: kSecondaryText,
          fontWeight: FontWeight.w300,
          fontSize: 16,
          fontFamily: 'IBMPlex',
          // shadows: [
          //   Shadow(
          //     color: Colors.black.withOpacity(0.25),
          //     offset: Offset.fromDirection(1.1, 2.75),
          //   )
          // ],
        ),
        headline1: base.textTheme.headline1?.copyWith(
          fontSize: 18,
          fontWeight: FontWeight.w500,
          fontFamily: 'IBMPlex',
          color: const Color(0x66ffffff),
          shadows: [
            Shadow(
              color: Colors.black.withOpacity(0.25),
              offset: Offset.fromDirection(1.1, 2.75),
            )
          ],
        ),
        headline2: base.textTheme.headline2?.copyWith(
          color: Colors.white.withOpacity(0.4),
          leadingDistribution: TextLeadingDistribution.even,
          fontWeight: FontWeight.w100,
          decorationColor: kPrimary,
          fontFamily: 'IBMPlex',
          fontSize: 24,
          shadows: [
            Shadow(
              color: Colors.black.withOpacity(0.25),
              offset: Offset.fromDirection(1.1, 2.75),
            )
          ],
        ),
      ),
      dataTableTheme: base.dataTableTheme.copyWith(
        decoration: const BoxDecoration(
          color: Colors.transparent,
          backgroundBlendMode: BlendMode.softLight,
        ),
        dataTextStyle: TextStyle(
          color: kSecondaryText,
          fontSize: 16,
          fontWeight: FontWeight.w400,
          fontFamily: 'IBMPlex',
          shadows: [
            Shadow(
              color: Colors.black.withOpacity(0.25),
              offset: Offset.fromDirection(1.1, 2.75),
            )
          ],
        ),
        headingTextStyle: base.textTheme.headline2!.copyWith(
          color: kSecondary.withOpacity(0.8),
          leadingDistribution: TextLeadingDistribution.even,
          fontWeight: FontWeight.w200,
          decorationColor: kPrimary.withOpacity(0.8),
          fontFamily: 'IBMPlex',
          fontSize: 24,
          shadows: [
            Shadow(
              color: Colors.black.withOpacity(0.25),
              offset: Offset.fromDirection(1.1, 2.75),
            )
          ],
        ),
      ),
      radioTheme: base.radioTheme.copyWith(
        fillColor:
            MaterialStateProperty.resolveWith((Set<MaterialState> states) {
          if (states.contains(MaterialState.selected)) {
            return kPrimary;
          }

          return kShadow;
        }),
        overlayColor: overlayState,
        // MaterialStateProperty.resolveWith((Set<MaterialState> states) {
        //   if (states.contains(MaterialState.selected)) {
        //     return kPrimary.withAlpha(0x1C);
        //   }
        //
        //   return kPrimary.withAlpha(kOverlayAlpha);
        // }),
      ),
      switchTheme: base.switchTheme.copyWith(
        thumbColor:
            MaterialStateProperty.resolveWith((Set<MaterialState> states) {
          if (states.contains(MaterialState.selected)) {
            return kPrimary;
          }

          return kShadow;
        }),
        trackColor:
            MaterialStateProperty.resolveWith((Set<MaterialState> states) {
          if (states.contains(MaterialState.selected)) {
            return kOnPrimary.withOpacity(0.15);
          }
          return kShadow;
          return Colors.grey.shade200.withAlpha(kOverlayAlpha);
        }),
        overlayColor: overlayState,
        // MaterialStateProperty.resolveWith((Set<MaterialState> states) {
        //   if (states.contains(MaterialState.selected)) {
        //     return kPrimary.withAlpha(0x1C);
        //   }
        //
        //   return kPrimary.withAlpha(kOverlayAlpha);
        // })),
      ),
      listTileTheme: base.listTileTheme.copyWith(
        horizontalTitleGap: 5,
        minVerticalPadding: 5,
        selectedColor: kPrimary.withOpacity(.75),
        contentPadding: const EdgeInsets.all(2),
        textColor: kPrimaryText,
        iconColor: kPrimary.withOpacity(0.8),
      ),
      popupMenuTheme: base.popupMenuTheme.copyWith(
        color: kLightBackground,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
        elevation: 8,
        enableFeedback: true,
      ),
      dialogTheme: base.dialogTheme.copyWith(
        backgroundColor: kLightBackground,
        elevation: 16,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
        contentTextStyle: base.dialogTheme.contentTextStyle?.copyWith(
          color: kPrimaryText,
          fontFamily: 'IBMPlex',
        ),
        titleTextStyle: base.dialogTheme.titleTextStyle?.copyWith(
          color: kPrimaryText,
          fontFamily: 'IBMPlex',
        ),
      ),
      timePickerTheme: TimePickerTheme.of(context).copyWith(
        backgroundColor: kBackground,
        hourMinuteShape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        dayPeriodShape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
      ),
      primaryIconTheme: base.primaryIconTheme.copyWith(
        color: kPrimaryIcon,
      ),
      appBarTheme: base.appBarTheme.copyWith(
        titleTextStyle: titleTextStyle,
        iconTheme: iconTheme,
        actionsIconTheme: iconTheme,
        foregroundColor: kOnPrimary,
        backgroundColor: kBackground.withOpacity(0.75),
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.dark,
          systemNavigationBarDividerColor: Colors.white.withOpacity(.25),
          systemNavigationBarColor: kOverlay,
        ),
      ),
      iconTheme: const IconThemeData(
        color: kPrimaryIcon,
        size: 32,
      ),
      inputDecorationTheme: base.inputDecorationTheme.copyWith(
        labelStyle: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: Colors.white,
        ),
        fillColor: kBackground,
        focusColor: kSecondary,
        helperStyle: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w300,
        ),
        border: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
          ),
        ),
      ),
      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: Colors.white.withOpacity(0.75),
        splashColor: kLightBackground,
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          overlayColor: overlayState,
          backgroundColor: btnBackground,
          textStyle:
              MaterialStateProperty.resolveWith((Set<MaterialState> states) {
            return base.textTheme.caption!.copyWith(
              height: 50,
            );
          }),
        ),
      ),
      bottomSheetTheme: base.bottomSheetTheme.copyWith(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12.0),
                topRight: Radius.circular(12.0)),
          ),
          modalElevation: 16,
          // modalBackgroundColor: kBackground,
          clipBehavior: Clip.antiAlias,
          backgroundColor: kLightBackground.withOpacity(0.95),
          constraints: const BoxConstraints(
            maxWidth: 460,
          )));
}

ThemeData theme(BuildContext context, [bool isProviderSet = false]) =>
    _buildTheme(context, isProviderSet);

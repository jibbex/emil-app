import 'package:flutter/material.dart';

class EmilAppLocalizations {
  final Locale locale;

  EmilAppLocalizations(this.locale);

  static EmilAppLocalizations? of(BuildContext context) {
    return Localizations.of<EmilAppLocalizations>(context, EmilAppLocalizations);
  }

  static const _localizedValues = <String, Map<String, String>>{
    "en": {
      "appTitle": "Extended Mathematical Improvement and Learning",
      "appShortTitle": "E.M.I.L.",
      "difficultyTitle": "Difficulties",
      "difficultyEasy": "Easy",
      "difficultyMedium": "Medium",
      "difficultyHard": "Hard",
      "addition": "add",
      "subtract": "subtract",
      "multiply": "multiply",
      "divide": "divide",
      "operations": "Arithmetic operations.",
      "add": "add",
      "remove": "remove",
      "edit": "edit",
      "timer": "Time attack",
      "range": "range",
      "hallOfFame": "High Scores",
      "multiplayer": "Multiplayer",
      "multiplayer_info": "Scan QR code to join.",
      "multiplayer_idle": "Waiting for players",
      "player_title": "Player",
      "select_avatar": "Select avatar",
      "avatar": "Avatar",
      "join": "join",
      "createGame": "create Game",
      "start": "start",
      "on": "on",
      "off": "off",
      "of": "of",
      "error": "Error",
      "notEmpty": "must not be empty",
      "ok": "ok",
      "correct": "are correct",
      "settingsTitle": "Settings",
      "animatedBg": "Animated background",
      "bgShader": "Background shader",
      "glassMorphism": "Frosted glass",
      "effects": "Effects",
      "gameplayTitle": "Gameplay",
      "opacity": "Transparency",
      "high_scores_fastest": "fastest",
      "high_scores_timing": "time attack",
      "connection_error": "Connection failed.",
      "invite_player": "Invite player",
      "retry": "retry",
      "scanning": "Scanning",
      "correct_mistakes": "Correct mistakes directly",
      "name": "Name",
      "time": "Time",
      "scores": "Scores",
      "tasks": "Tasks",
      "about": "About",
      "legal": "© 2022 michm.de",
      "version": "v0.5.0",
      "versionDisclaimer": "This is an early development build and it will have bugs.",
    },
    "de": {
      "appTitle": "Extended Mathematical Improvement and Learning",
      "appShortTitle": "E.M.I.L.",
      "difficultyTitle": "Schwierigkeit",
      "difficultyEasy": "Einfach",
      "difficultyMedium": "Mittel",
      "difficultyHard": "Schwer",
      "addition": "addieren",
      "subtract": "subtrahieren",
      "multiply": "multiplizieren",
      "divide": "dividieren",
      "operations": "Arithmetic operations.",
      "add": "hinzufügen",
      "remove": "entfernen",
      "edit": "bearbeiten",
      "timer": "Gegen die Uhr",
      "range": "Bereich",
      "hallOfFame": "Bestenliste",
      "multiplayer": "Mehrspieler",
      "multiplayer_info": "QR Code scannen um teilzunehmen.",
      "multiplayer_idle": "Warte auf Mitspieler",
      "player_title": "Spieler",
      "select_avatar": "Avatar wählen",
      "avatar": "Avatar",
      "join": "teilnehmen",
      "createGame": "Spiel erstellen",
      "start": "Starten",
      "on": "an",
      "off": "aus",
      "of": "von",
      "error": "Fehler",
      "notEmpty": "darf nicht leer sein",
      "ok": "ok",
      "correct": "sind richtig",
      "settingsTitle": "Einstellungen",
      "animatedBg": "Animierter Hintergrund",
      "bgShader": "Hintergrund Shader",
      "glassMorphism": "Gefrorenes Glas",
      "effects": "Effekte",
      "gameplayTitle": "Gameplay",
      "opacity": "Transparenz",
      "name": "Name",
      "time": "Zeit",
      "scores": "Punkte",
      "tasks": "Aufgaben",
      "about": "Über",
      "retry": "Erneut versuchen",
      "scanning": "Scannen",
      "high_scores_fastest": "schnellste",
      "high_scores_timing": "time attack",
      "connection_error": "Verbindung ist fehlgeschlagen.",
      "invite_player": "Spieler einladen",
      "correct_mistakes": "Fehler direkt korrigieren",
      "legal": "© 2022 michm.de",
      "version": "v0.5.0",
      "versionDisclaimer": "This is an early development build and it will have bugs.",
    }
  };

  static List<String> languages () => _localizedValues.keys.toList();

  String get appTitle {
    return _localizedValues[locale.languageCode]!["appTitle"]!;
  }

  String get appShortTitle {
    return _localizedValues[locale.languageCode]!["appShortTitle"]!;
  }

  String get difficultyTitle {
    return _localizedValues[locale.languageCode]!["difficultyTitle"]!;
  }

  String get difficultyEasy {
    return _localizedValues[locale.languageCode]!["difficultyEasy"]!;
  }

  String get difficultyMedium {
    return _localizedValues[locale.languageCode]!["difficultyMedium"]!;
  }

  String get difficultyHard {
    return _localizedValues[locale.languageCode]!["difficultyHard"]!;
  }

  String get addition {
    return _localizedValues[locale.languageCode]!["addition"]!;
  }

  String get subtract {
    return _localizedValues[locale.languageCode]!["subtract"]!;
  }

  String get multiply {
    return _localizedValues[locale.languageCode]!["multiply"]!;
  }

  String get divide {
    return _localizedValues[locale.languageCode]!["divide"]!;
  }

  String get operations {
    return _localizedValues[locale.languageCode]!["operations"]!;
  }

  String get add {
    return _localizedValues[locale.languageCode]!["add"]!;
  }

  String get remove {
    return _localizedValues[locale.languageCode]!["remove"]!;
  }

  String get edit {
    return _localizedValues[locale.languageCode]!["edit"]!;
  }

  String get timer {
    return _localizedValues[locale.languageCode]!["timer"]!;
  }

  String get range {
    return _localizedValues[locale.languageCode]!["range"]!;
  }

  String get hallOfFame {
    return _localizedValues[locale.languageCode]!["hallOfFame"]!;
  }

  String get multiplayer {
    return _localizedValues[locale.languageCode]!["multiplayer"]!;
  }

  String get join {
    return _localizedValues[locale.languageCode]!["join"]!;
  }

  String get createGame {
    return _localizedValues[locale.languageCode]!["createGame"]!;
  }

  String get start {
    return _localizedValues[locale.languageCode]!["start"]!;
  }

  String get on {
    return _localizedValues[locale.languageCode]!["on"]!;
  }

  String get off {
    return _localizedValues[locale.languageCode]!["off"]!;
  }

  String get getOf {
    return _localizedValues[locale.languageCode]!["of"]!;
  }

  String get correct {
    return _localizedValues[locale.languageCode]!["correct"]!;
  }

  String get settingsTitle {
    return _localizedValues[locale.languageCode]!["settingsTitle"]!;
  }

  String get animatedBg {
    return _localizedValues[locale.languageCode]!["animatedBg"]!;
  }

  String get glassMorphism {
    return _localizedValues[locale.languageCode]!["glassMorphism"]!;
  }

  String get effects {
    return _localizedValues[locale.languageCode]!["effects"]!;
  }

  String get gameplayTitle {
    return _localizedValues[locale.languageCode]!["gameplayTitle"]!;
  }

  String get opacity {
    return _localizedValues[locale.languageCode]!["opacity"]!;
  }

  String get name {
    return _localizedValues[locale.languageCode]!["name"]!;
  }

  String get time {
    return _localizedValues[locale.languageCode]!["time"]!;
  }

  String get scores {
    return _localizedValues[locale.languageCode]!["scores"]!;
  }

  String get tasks {
    return _localizedValues[locale.languageCode]!["tasks"]!;
  }

  String get about {
    return _localizedValues[locale.languageCode]!["about"]!;
  }

  String get legal {
    return _localizedValues[locale.languageCode]!["legal"]!;
  }

  String get version {
    return _localizedValues[locale.languageCode]!["version"]!;
  }

  String get versionDisclaimer {
    return _localizedValues[locale.languageCode]!["versionDisclaimer"]!;
  }

  String get error {
    return _localizedValues[locale.languageCode]!["error"]!;
  }

  String get notEmpty {
    return _localizedValues[locale.languageCode]!["notEmpty"]!;
  }

  String get ok {
    return _localizedValues[locale.languageCode]!["ok"]!;
  }

  String get highScoresFastest {
    return _localizedValues[locale.languageCode]!["high_scores_fastest"]!;
  }

  String get highScoresTimeAttack {
    return _localizedValues[locale.languageCode]!["high_scores_timing"]!;
  }

  String get multiplayerInfo {
    return _localizedValues[locale.languageCode]!["multiplayer_info"]!;
  }

  String get connectionError {
    return _localizedValues[locale.languageCode]!["connection_error"]!;
  }

  String get retry {
    return _localizedValues[locale.languageCode]!["retry"]!;
  }

  String get scanning {
    return _localizedValues[locale.languageCode]!["scanning"]!;
  }

  String get bgShader {
    return _localizedValues[locale.languageCode]!["bgShader"]!;
  }

  String get mpIdle {
    return _localizedValues[locale.languageCode]!["multiplayer_idle"]!;
  }

  String get invite {
    return _localizedValues[locale.languageCode]!["invite_player"]!;
  }

  String get playerTitle {
    return _localizedValues[locale.languageCode]!["player_title"]!;
  }

  String get avatar {
    return _localizedValues[locale.languageCode]!["avatar"]!;
  }

  String get selectAvatar {
    return _localizedValues[locale.languageCode]!["select_avatar"]!;
  }

  String get correctMistakes {
    return _localizedValues[locale.languageCode]!["correct_mistakes"]!;
  }
}

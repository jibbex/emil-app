import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class EmilAppLocalizationsDelegate extends LocalizationsDelegate<EmilAppLocalizations> {
  EmilAppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => EmilAppLocalizations.languages().contains(locale.languageCode);

  @override
  Future<EmilAppLocalizations> load(Locale locale) {
    return SynchronousFuture<EmilAppLocalizations>(EmilAppLocalizations(locale));
  }

  @override
  bool shouldReload(EmilAppLocalizationsDelegate old) => false;
}
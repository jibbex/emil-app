import 'dart:io';
import 'package:path/path.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:localstorage/localstorage.dart';
import 'package:path_provider/path_provider.dart';

/// Creates and initializes a LocalStorage object
/// depending on the platform.
extension StorageFactory on LocalStorage {
  /// Ignores the filepath parameter on web, adds on
  /// non web platforms a file extension (.json) and
  /// returns the initialized LocalStorage object.
  static Future<LocalStorage> init(String filename, [String? filepath]) async {
      LocalStorage? storage;

      if (kIsWeb) {
        storage = LocalStorage(filename);
      } else if (filepath == null) {
        storage = LocalStorage('$filename.json');
      } else {
        Directory appPath = await getApplicationSupportDirectory();
        storage = LocalStorage('$filename.json', join(appPath.path, filepath));
      }

      await storage.ready;

      return storage;
  }


}
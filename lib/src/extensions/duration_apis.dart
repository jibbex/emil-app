extension FormattedString on Duration {
  /// Returns an "hh:mm:ss" formatted String
  String get formattedString {
    String buffer = toString();
    int len = buffer.length;
    return buffer.substring(0, len - 7);
  }
}
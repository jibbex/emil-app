import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/models/high_scores/high_scores.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/widgets/glass_card.dart';
import 'package:emil_app/src/widgets/score_data_table.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HighScorePage extends StatelessWidget {
  final HighScores scores;

  const HighScorePage({
    super.key,
    required this.scores,
  });

  @override
  Widget build(BuildContext context) {
    final config = context.read<Configuration>();
    final spacing = getContainerSpacing(context);

    return GlassCard(
      margin: spacing,
      borderRadius: 16,
      backgroundColor: Colors.white.withOpacity(config.opacity),
      child: ScoreDataTable(
        scores: scores,
      ),
    );
  }
}

import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/widgets/emil_scaffold.dart';
import 'package:emil_app/src/widgets/glass_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StartView extends StatelessWidget {
  final SliverAppBar? appBar;

  const StartView({
    super.key,
    this.appBar,
  });

  @override
  Widget build(BuildContext context) {
    const kButtonSize = Size(300, 85);
    final Configuration config = context.watch<Configuration>();
    final deviceHeight = MediaQuery.of(context).size.height;

    return EmilScaffold(
      child: CustomScrollView(
        slivers: <Widget>[
          if (appBar != null) appBar!,
          SliverList(
            delegate: SliverChildListDelegate([
              SizedBox(
                height: deviceHeight - 100,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GlassButton(
                        width: kButtonSize.width,
                        height: kButtonSize.height,
                        glass: config.isGlassMorphismActivated,
                        onPressed: () =>
                            Navigator.of(context).pushNamed('/play'),
                        child: Center(
                          child: Text(
                            EmilAppLocalizations.of(context)!
                                .start,
                            style: gmBtnTextStyle,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      GlassButton(
                        width: kButtonSize.width,
                        height: kButtonSize.height,
                        glass: config.isGlassMorphismActivated,
                        onPressed: () =>
                            Navigator.of(context).pushNamed('/multiplayer'),
                        child: Center(
                          child: Text(
                            EmilAppLocalizations.of(context)!
                                .multiplayer,
                            style: gmBtnTextStyle,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      GlassButton(
                        width: kButtonSize.width,
                        height: kButtonSize.height,
                        glass: config.isGlassMorphismActivated,
                        onPressed: () =>
                            Navigator.of(context).pushNamed('/highscores'),
                        child: Center(
                          child: Text(
                            EmilAppLocalizations.of(context)!
                                .hallOfFame,
                            style: gmBtnTextStyle,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/views/calculation_view.dart';
import 'package:emil_app/src/widgets/centered_container.dart';
import 'package:emil_app/src/widgets/app_bar.dart';
import 'package:emil_app/src/widgets/app_bar_button.dart';
import 'package:emil_app/src/widgets/emil_scaffold.dart';
import 'package:emil_app/src/ws_client.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class QrScanView extends StatefulWidget {
  final SliverAppBar? appBar;
  final String baseUrl;

  const QrScanView({
    super.key,
    this.appBar,
    this.baseUrl = 'ws://localhost:8080/socket',
  });

  @override
  State<QrScanView> createState() => _QrScanViewState();
}

class _QrScanViewState extends State<QrScanView> {
  int? latency;
  bool _idle = false;
  Configuration? _config;
  late final WsClient _client;

  void _onConnectionError(Object error) {
    String code = "0";
    const kEmpty = '';
    final localizations = EmilAppLocalizations.of(context)!;
    final errMsg = error.toString();
    final matches = RegExp(r'errno = (\d+)').allMatches(errMsg);
    final message = errMsg
        .replaceAll(RegExp(r'.+Exception: '), kEmpty)
        .replaceAll(RegExp(r'\(.+\).+', multiLine: true, dotAll: true), kEmpty);

    try {
      if (matches.first.groupCount == 1) {
        code = matches.first.group(1)!;
      }
    } catch (error) {
      if (kDebugMode) {
        rethrow;
      }
    }

    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 0,
                top: 2.5,
                bottom: 2.5,
                right: 10,
              ),
              child: Icon(
                Icons.error_rounded,
                size: 32,
                color: Colors.black.withOpacity(0.5),
              ),
            ),
            Expanded(
              child: Text('${localizations.error}: $code'),
            ),
          ],
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              localizations.connectionError,
              textAlign: TextAlign.left,
              style: Theme.of(context).textTheme.headline3!.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.italic,
                    color: kPrimaryText.withOpacity(0.85),
                  ),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(message),
          ],
        ),
        actions: <Widget>[
          TextButton(
            onPressed: _reconnect,
            child: Text(localizations.retry.toUpperCase()),
          ),
          TextButton(
            onPressed: _popBack,
            child: Text(localizations.ok.toUpperCase()),
          ),
        ],
      ),
    );
  }

  void _toCalculationView() {
    _client.channel.then(
      (channel) => Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (BuildContext context) {
          EmilAppLocalizations locale = EmilAppLocalizations.of(context)!;
          double width = MediaQuery.of(context).size.width;

          return CalculationView(
            isMultiplayerSession: true,
            channel: channel,
            baseUrl: widget.baseUrl,
            appBar: appBar(
              context: context,
              title: width > 960 ? locale.appTitle : locale.appShortTitle,
              leading: AppBarButton(
                const Icon(Icons.arrow_back_ios_new),
                onPressed: () => Navigator.of(context).pop(),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  void _popBack() {
    Navigator.pop(context);
    Navigator.pop(context);
  }

  void _reconnect() {
    Navigator.pop(context);
    _connect();
  }

  void _onData(Command cmd) {
    if (cmd.type == CommandType.ready) {
      setState(() => _idle = true);
    } else if (cmd.type == CommandType.goto) {
      if (cmd.payload!['view'] == 'CalculationView') {
        _toCalculationView();
      }
    }
  }

  void _connect([String? channel]) async {
    _client.connect(channel);
    await _client.listen(_onData, onError: _onConnectionError);
    _client.send(CommandType.sync);
  }

  void _onDetectQr(BarcodeCapture capture) async {
    String? value;
    final List<Barcode> barcodes = capture.barcodes;

    for (final barcode in barcodes) {
      if ((value = barcode.rawValue) != null) {
        final channel = value;
        ToastContext().init(context);
        Toast.show(channel ?? 'Invalid data', duration: 3, gravity: Toast.bottom);
        _connect(channel);
        break;
      }
    }
  }

  Widget _futureBuilder(BuildContext context, AsyncSnapshot<String> snapshot) {
    final locale = EmilAppLocalizations.of(context)!;

    if (snapshot.hasError) {
      _onConnectionError(snapshot.error!);
    }

    return CenteredContainer(
      height: 256,
      config: _config!,
      child: _idle
          ? Center(
              child: Text(
                EmilAppLocalizations.of(context)!.mpIdle,
                textAlign: TextAlign.center,
                style: gmBtnTextStyle,
              ),
            )
          : MobileScanner(
              controller: MobileScannerController(
                detectionTimeoutMs: 1000,
              ),
              startDelay: true,
              fit: BoxFit.cover,
              onDetect: _onDetectQr,
            ),
    );
  }

  @override
  void initState() {
    _client = WsClient(widget.baseUrl);
    super.initState();
  }

  @override
  void dispose() {
    _client.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _config ??= context.watch<Configuration>();

    return EmilScaffold(
      child: CustomScrollView(
        slivers: <Widget>[
          if (widget.appBar != null) widget.appBar!,
          SliverList(
            delegate: SliverChildListDelegate([
              FutureBuilder<String>(
                future: _client.id,
                builder: _futureBuilder,
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

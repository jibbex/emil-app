import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/models/high_scores/high_scores.dart';
import 'package:emil_app/src/models/player.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/views/high_score_page.dart';
import 'package:emil_app/src/widgets/emil_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:blur_bottom_bar/blur_bottom_bar.dart';
import 'package:provider/provider.dart';

class HighScoresView extends StatefulWidget {
  final SliverAppBar? appBar;
  final Player? player;

  const HighScoresView({
    super.key,
    this.appBar,
    this.player,
  });

  @override
  State<HighScoresView> createState() => _HighScoresViewState();
}

class _HighScoresViewState extends State<HighScoresView> {
  int _pageIndex = -1;
  Configuration? _config;
  final List<String> _stores = ['high_scores', 'time_scores'];

  TextStyle? getTextStyle(BuildContext context) =>
      Theme.of(context).textTheme.bodyLarge?.copyWith(
            fontWeight: FontWeight.w400,
            color: Colors.white70.withOpacity(0.6),
          );

  TextStyle? getTitleStyle(BuildContext context) =>
      getTextStyle(context)?.copyWith(
        fontSize: 16,
        fontWeight: FontWeight.w200,
      );

  @override
  Widget build(BuildContext context) {
    _config ??= context.watch<Configuration>();
    var localizations = EmilAppLocalizations.of(context)!;
    var data = context.watch<HighScores>();

    if (_pageIndex == -1) {
      setState(() => _pageIndex = data.scoreIndex);
    }

    return EmilScaffold(
      bottomNavBar: BlurBottomView(
        backgroundColor: kOverlay.withOpacity(0.5),
        bottomNavigationBarItems: [
          BottomNavigationBarItem(
            icon: const Icon(Icons.score_rounded),
            label: localizations.highScoresFastest,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.timelapse_rounded),
            label: localizations.highScoresTimeAttack,
          ),
        ],
        currentIndex: _pageIndex,
        onIndexChange: (int index) {
          setState(() => _pageIndex = index);
          data.loadData(_stores[_pageIndex]);
        },
        unselectedItemColor: kBackground.withOpacity(0.8),
        selectedItemColor: kPrimaryIcon.withOpacity(0.8),
        showSelectedLabels: true,
      ),
      child: CustomScrollView(
        slivers: <Widget>[
          if (widget.appBar != null) widget.appBar!,
          SliverList(
            delegate: SliverChildListDelegate(
              [
                const SizedBox(
                  height: 20,
                ),
                HighScorePage(scores: data),
                const SizedBox(
                  height: 80,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/widgets/emil_scaffold.dart';
import 'package:emil_app/src/ws_client.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class JoinView extends StatefulWidget {
  final String channel;
  final String baseUrl;
  final SliverAppBar? appBar;

  const JoinView({
    super.key,
    this.appBar,
    required this.baseUrl,
    required this.channel,
  });

  @override
  State<JoinView> createState() => _JoinViewState();
}

class _JoinViewState extends State<JoinView> {
  late WsClient _client;
  late Configuration _config;

  @override
  void initState() {
    _client = WsClient(widget.baseUrl);
    super.initState();
  }

  @override
  void dispose() {
    _client.close();
    super.dispose();
  }

  Widget _futureBuilder(BuildContext context, AsyncSnapshot<String> snapshot) {
    final size = MediaQuery.of(context).size;
    final spacing = getContainerSpacing(context);
    final locale = EmilAppLocalizations.of(context)!;

    // if (snapshot.hasError) {
    //   _onConnectionError(snapshot.error!);
    // }

    return SizedBox(
      height: 128,
      width: 128,
      child: CircularProgressIndicator(
        backgroundColor: Colors.transparent,
        strokeWidth: 14,
        color: kPrimary.withOpacity(0.25),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: not here ...
    _config = context.watch<Configuration>();

    return EmilScaffold(
      child: CustomScrollView(
        slivers: <Widget>[
          if (widget.appBar != null) widget.appBar!,
          SliverList(
            delegate: SliverChildListDelegate([
              FutureBuilder<String>(
                future: _client.id,
                builder: _futureBuilder,
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

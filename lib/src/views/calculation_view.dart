import 'dart:async';
import 'dart:math';
import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/models/difficulty/difficulty.dart';
import 'package:emil_app/src/models/player.dart';
import 'package:emil_app/src/models/task.dart';
import 'package:emil_app/src/models/tasks.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/views/game_over_view.dart';
import 'package:emil_app/src/widgets/emil_scaffold.dart';
import 'package:emil_app/src/widgets/glass_card.dart';
import 'package:emil_app/src/widgets/num_pad.dart';
import 'package:emil_app/src/ws_client.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';

enum GameState { init, idle, ready, over }

class CalculationView extends StatefulWidget {
  final bool isMultiplayerSession;
  final SliverAppBar? appBar;
  final PeerType? peerType;
  final String? baseUrl;
  final String? channel;

  const CalculationView({
    super.key,
    this.appBar,
    this.channel,
    this.baseUrl,
    this.peerType = PeerType.secondary,
    this.isMultiplayerSession = false,
  });

  @override
  State<CalculationView> createState() => _CalculationViewState();
}

class _CalculationViewState extends State<CalculationView>
    with TickerProviderStateMixin {
  String _equation = '0 + 0';
  bool isListening = false;
  bool _showCheckmark = true;
  DateTime startTime = DateTime.now();
  WsClient? _client;
  GameState _state = GameState.init;
  late Duration? _duration;
  late Configuration? _config;
  late List<Player> _players;
  late final bool isPrimaryPeer;
  late final bool isSecondaryPeer;
  late final bool isMultiplayerSession;
  final Completer<Tasks> _taskCompleter = Completer();
  static const int kCountdown = 5;

  /// Currently timer mode is not supported on a multiplayer game.
  late bool _isTimerActive;

  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 1),
    vsync: this,
  );

  late final AnimationController _offsetController = AnimationController(
    duration: const Duration(milliseconds: 750),
    vsync: this,
  );

  late final Animation<double> _animation = CurvedAnimation(
    parent: _controller,
    curve: Curves.fastOutSlowIn,
  );

  late final Animation<Offset> _offsetAnimation = Tween(
    begin: const Offset(0.0, 2.5),
    end: Offset.zero,
  ).animate(CurvedAnimation(
    parent: _offsetController,
    curve: Curves.fastOutSlowIn,
  ));

  late Timer? _interval;

  @override
  void initState() {
    isMultiplayerSession = widget.isMultiplayerSession;
    isPrimaryPeer = widget.peerType == PeerType.primary;
    isSecondaryPeer = widget.peerType == PeerType.secondary;

    _config = null;
    _duration = null;
    _interval = null;
    _players = [];

    if (isMultiplayerSession) {
      _client = WsClient(widget.baseUrl!);
      _client!.connect(widget.channel);
      _client!.listen(_onData);

      if (widget.peerType == PeerType.secondary) {
        _client!.send(CommandType.req);
      }

      _isTimerActive = false;
    }

    super.initState();
  }

  @override
  void dispose() {
    _client?.send(CommandType.end);
    _controller.dispose();
    _offsetController.dispose();
    _interval?.cancel();
    _client?.close();
    super.dispose();
  }

  void _onData(Command cmd) async {
    final isNotThis = cmd.payload?['id'] != _client?.id;

    if (kDebugMode) {
      print('${widget.peerType}: ${cmd.type} (${cmd.payload})');
    }

    if (cmd.type == CommandType.task &&
        isSecondaryPeer &&
        cmd.payload?['task'] != null) {
      _players[0].tasks.add(Task()..expression = cmd.payload!['task']);
    } else if (cmd.type == CommandType.start) {
      // TODO: receive and send avatar image
      // final image = await _players[0].avatar.toBase64();
      // final command = Command(CommandType.player, {
      //   'player': _players[0].toMap()..addAll({'avatarImage': image})
      // });
      //
      // if (kDebugMode) {
      //   print(_players[0].toMap());
      //   print(_players[0]);
      //   print(cmd.payload?['player']);
      // }

      Future.delayed(const Duration(seconds: kCountdown)).then((_) {
        setState(() {
          if (isSecondaryPeer) {
            _players[0].tasks.moveNext();
          }

          _equation = _players[0].tasks.current.toString();
          _state = GameState.ready;
        });
      });
    } else if (cmd.type == CommandType.req && isPrimaryPeer) {
      int i = 0;

      _client?.send(Command(CommandType.task, {
        'length': _players[0].tasks.length,
      }));

      for (final task in (await _taskCompleter.future)) {
        final cmd = Command(
          CommandType.task,
          {'task': task.toString()},
        )..append(MapEntry('index', i++));
        _client?.send(cmd);
      }

      Future.delayed(
        Duration(
          seconds: kCountdown,
          milliseconds: _client!.latency,
        ),
      ).then((_) async {
        // TODO: receive and send avatar image
        // final image = await _players[0].avatar.toBase64();
        _client?.send(Command(CommandType.player, {
          'seconds': kCountdown,
          'player': _players[0].toMap(),
        }));
        setState(() => _state = GameState.ready);
      });
    } else if (cmd.type == CommandType.done && isNotThis) {
      setState(() {
        _players[1]
            .finishedTasks
            .add(Task()..expression = cmd.payload!['task']);
      });
    } else if (cmd.type == CommandType.end) {
      Navigator.of(context).pop();
    } else if (cmd.type == CommandType.player) {
      if (kDebugMode) {
        print(cmd.payload!['player']);
      }

      if (isSecondaryPeer) {
        _client?.send(Command(CommandType.player, {
          'player': _players[0].toMap(),
        }));
      }

      setState(() {
        _players.add(Player.fromMap(cmd.payload!['player']));
        _state = GameState.ready;

        if (isSecondaryPeer) {
          _players[0].tasks.moveNext();
          _equation = '${_players[0].tasks.current}';
        }
      });
    } else if (cmd.type == CommandType.gameOver && cmd.payload != null) {
      _players.add(Player.fromMap(cmd.payload!['player']));
      if (_players.length == 2) {
        _showGameOverView();
      }
    } else if (cmd.type == CommandType.goto) {
      _showGameOverView();
    }
  }

  Future<void> _playCheckAnimation() async {
    _controller.reset();
    _offsetController.reset();

    await Future.any([
      _controller.forward(),
      _offsetController.forward(),
    ]);

    _controller.reverse();
  }

  Widget _progressBuilder(BuildContext context) {
    if (!_isTimerActive && _state != GameState.idle) {
      return GlassCard(
        borderWidth: 0,
        backgroundColor: Colors.black38.withOpacity(0.05),
        margin: const EdgeInsets.only(top: 25),
        children: _players
            .map((Player e) => Column(children: [
                  Center(
                    child: LinearProgressIndicator(
                      minHeight: 10,
                      value: e.finishedTasks.length /
                          (e.tasks.isEmpty ? 1 : e.tasks.length),
                      backgroundColor: Colors.transparent,
                      color: kOnPrimary.withOpacity(0.2),
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                ]))
            .toList(),
      );
    }

    final int total = _config!.selectedDifficulty.timer!.value!.inSeconds;
    final double progress = _duration!.inSeconds / total;

    _interval ??= Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_duration!.inSeconds > 0) {
        setState(() {
          _duration = Duration(
            seconds: _duration!.inSeconds - 1,
          );
        });
      } else {
        timer.cancel();
        _showGameOverView();
      }
    });

    return Container(
      constraints: const BoxConstraints(
        maxWidth: 150,
        maxHeight: 112,
      ),
      child: GlassCard(
        borderRadius: 16,
        backgroundColor: Colors.white.withOpacity(_config!.opacity),
        margin: const EdgeInsets.only(top: 15),
        children: [
          CircularPercentIndicator(
            animateFromLastPercent: true,
            animation: true,
            circularStrokeCap: CircularStrokeCap.round,
            backgroundColor: Colors.transparent,
            startAngle: 360,
            center: Icon(
              Icons.timer_rounded,
              size: 64,
              color: Colors.white.withOpacity(0.2),
            ),
            radius: 45,
            percent: progress,
            lineWidth: 8,
            progressColor: kPrimary.withOpacity(0.25),
          ),
        ],
      ),
    );
  }

  void _showGameOverView() {
    Navigator.of(context).pop();
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => GameOverView(
          players: _players,
          timingMode: _isTimerActive,
          appBar: SliverAppBar(
            title: widget.appBar!.title,
            leading: Padding(
              padding: const EdgeInsets.all(5),
              child: TextButton(
                onPressed: () => Navigator.of(context).pop(),
                style: TextButton.styleFrom(
                  foregroundColor: Colors.white.withOpacity(.5),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8),
                    ),
                  ),
                ),
                child: const Icon(Icons.arrow_back_ios_new_rounded),
              ),
            ),
            automaticallyImplyLeading: false,
            backgroundColor: Colors.transparent,
            foregroundColor: Colors.white.withOpacity(0.5),
            shadowColor: Colors.transparent,
          ),
        ),
      ),
    );
  }

  void _handleChanged(String value, Function? callback) async {
    final result = int.parse(value);
    final correctWrongResults =
        !isMultiplayerSession && _config!.isCorrectMistakesActive;

    _players[0].tasks.current.value = result;

    setState(() {
      _players[0].taskCount += 1;
      _players[0].finishedTasks.add(_players[0].tasks.current);
      _showCheckmark = _players[0].tasks.isCorrect();
    });

    if (isMultiplayerSession) {
      _client!.send(Command(CommandType.done, {
        'task': _players[0].tasks.current.toString(),
      }));
    }

    if (_players[0].tasks.isCorrect()) {
      _players[0].correctCount += 1;

      if (_isTimerActive) {
        setState(() {
          final int maxSec =
              _config!.selectedDifficulty.timer!.value!.inSeconds;
          final int bonus = _config!.selectedDifficulty.timer!.bonus!.inSeconds;
          final int seconds = _duration!.inSeconds + bonus;
          _duration = Duration(seconds: seconds > maxSec ? maxSec : seconds);
          _showCheckmark = true;
        });
      }
    }

    if (correctWrongResults || _players[0].tasks.isCorrect()) {
      _playCheckAnimation();
    }

    if (_players[0].tasks.moveNext()) {
      setState(() => _equation = _players[0].tasks.current.toString());
    } else if (_isTimerActive) {
      // TODO: Synchronisation of tasks with peer is required for multiplayer mode
      int pInd =
          Random().nextInt(_config!.selectedDifficulty.getPatterns.length);
      Difficulty difficulty = _config!.selectedDifficulty;
      _players[0].tasks.generate(20, difficulty[pInd]);
      _players[0].tasks.moveNext();
      setState(() => _equation = '${_players[0].tasks.current}');
    } else {
      if (isMultiplayerSession) {
        setState(() => _state = GameState.over);
        final img = await _players[0].avatar.toBase64();
        _client?.send(Command(
          CommandType.gameOver,
          {
            'player': _players[0].toMap()
              ..putIfAbsent('avatarImage', () => img ?? ''),
          },
        ));

        if (_players.length == 2) {
          _client?.send(CommandType.goto);
          _showGameOverView();
        }
      } else {
        _showGameOverView();
      }
    }

    if (callback != null) {
      callback();
    }
  }

  void _initialize() {
    if (_state == GameState.init && _config != null) {
      _players.add(
        Player.fromBasePlayer(_config!.player)
          ..startTime = DateTime.now()
          ..difficulty = _config!.getIndexOf(_config!.selectedDifficulty)
          ..finishedTasks = Tasks(),
      );

      if (!isMultiplayerSession || isPrimaryPeer) {
        int pInd = Random().nextInt(
          _config!.selectedDifficulty.getPatterns.length,
        );

        Difficulty difficulty = _config!.selectedDifficulty;

        setState(() {
          if (isMultiplayerSession) {
            _state = GameState.idle;
          }

          _players[0].tasks = Tasks(
            count: difficulty.taskCount,
            pattern: difficulty[pInd],
          );

          _taskCompleter.complete(_players[0].tasks);
          _players[0].tasks.moveNext();
          _equation = '${_players[0].tasks.current}';
          _state = isMultiplayerSession ? GameState.idle : GameState.ready;
          _duration = difficulty.timer!.value;

          if (_config!.isTimerActive && isMultiplayerSession) {
            _isTimerActive = false;
          } else {
            _isTimerActive = _config!.isTimerActive;
          }
        });
      }
    } else if (_state == GameState.init && _config != null && isSecondaryPeer) {
      setState(() {
        _players[0].tasks = Tasks();
        _state = GameState.idle;
      });
    }
  }

  Widget _loadingBuilder(size, spacing) => SizedBox(
        height: size.height - 100,
        width: size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GlassCard(
              height: 256,
              margin: spacing,
              borderRadius: 16,
              backgroundColor: Colors.white.withOpacity(_config!.opacity),
              children: [
                const Spacer(),
                _state != GameState.over
                    ? Center(
                        child: SizedBox(
                          height: 128,
                          width: 128,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.transparent,
                            strokeWidth: 14,
                            color: kPrimary.withOpacity(0.25),
                          ),
                        ),
                      )
                    : Center(
                        child: Text(
                          EmilAppLocalizations.of(context)!.mpIdle,
                          textAlign: TextAlign.center,
                          style: gmBtnTextStyle,
                        ),
                      ),
                const Spacer(),
              ],
            ),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final spacing = getContainerSpacing(context);
    double iconSize = size.width < size.height ? size.width : size.height;

    if (_config == null) {
      setState(() {
        _config = context.watch<Configuration>();
      });
    }

    _initialize();

    return EmilScaffold(
      backgroundChild: Center(
        child: ScaleTransition(
          scale: _animation,
          child: FadeTransition(
            opacity: _animation,
            child: SlideTransition(
              position: _offsetAnimation,
              child: ShaderMask(
                shaderCallback: (bounds) {
                  return const LinearGradient(
                      transform: GradientRotation(-45),
                      colors: [
                        Colors.white10,
                        Colors.white38,
                        Colors.white10
                      ]).createShader(bounds);
                },
                child: _showCheckmark
                    ? Icon(
                        Icons.check_rounded,
                        size: iconSize,
                        color: kOnPrimary.withOpacity(0.8),
                      )
                    : Text(
                        _players[0].finishedTasks.last.result.toString(),
                        style: TextStyle(
                          fontSize: iconSize * 0.6,
                          color: kOnPrimary,
                        ),
                      ),
              ),
            ),
          ),
        ),
      ),
      child: CustomScrollView(
        slivers: <Widget>[
          if (widget.appBar != null) widget.appBar!,
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  margin: getContainerSpacing(context),
                  child: _state != GameState.ready
                      ? _loadingBuilder(size, spacing)
                      : Column(
                          children: [
                            NumPad(
                              onChanged: _handleChanged,
                              equation: _equation,
                            ),
                            _progressBuilder(context),
                          ],
                        ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

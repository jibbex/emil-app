import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/models/difficulty/difficulty.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/widgets/avatar_select.dart';
import 'package:emil_app/src/widgets/emil_scaffold.dart';
import 'package:emil_app/src/widgets/glass_card.dart';
import 'package:emil_app/src/widgets/text_field_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConfigurationView extends StatefulWidget {
  final SliverAppBar? appBar;

  const ConfigurationView({
    super.key,
    this.appBar,
  });

  @override
  State<ConfigurationView> createState() => _ConfigurationViewState();
}

class _ConfigurationViewState extends State<ConfigurationView> {
  Configuration? _config;
  final logo = Image.asset('assets/icon.png').image;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(logo, context);
  }

  Widget _listTile({
    required Widget child,
    BorderRadiusGeometry? borderRadius,
  }) =>
      Material(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: borderRadius != null
            ? RoundedRectangleBorder(
                borderRadius: borderRadius,
              )
            : null,
        color: Colors.white.withOpacity(0.00),
        child: child,
      );

  Widget aboutTile(BuildContext context) {
    EmilAppLocalizations localizations = EmilAppLocalizations.of(context)!;
    Size screenSize = MediaQuery.of(context).size;
    TextStyle style = Theme.of(context).textTheme.bodyText1!.copyWith(
      leadingDistribution: TextLeadingDistribution.even,
      fontWeight: FontWeight.w400,
      decorationColor: kPrimary,
      fontSize: 18,
      shadows: [
        Shadow(
          color: Colors.black.withOpacity(0.05),
          offset: Offset.fromDirection(1.1, 2.75),
        )
      ],
    );

    if (screenSize.width < 800) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image(
            image: logo,
            width: screenSize.width * 0.25,
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            localizations.versionDisclaimer,
            style: style,
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            localizations.legal,
            style: style,
            textAlign: TextAlign.center,
          ),
          Text(
            localizations.version,
            style: style,
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      );
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Spacer(),
        Image(
          image: Image.asset('assets/icon.png').image,
          width: screenSize.width * 0.1,
        ),
        const Spacer(),
        Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Text(
            localizations.versionDisclaimer,
            style: style,
          ),
          Text(
            localizations.legal,
            style: style,
          ),
          Text(
            localizations.version,
            style: style,
          ),
        ]),
        const Spacer(),
      ],
    );
  }

  _text(String text, Color color) => Text(
        text,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: color,
            ),
      );

  @override
  Widget build(BuildContext context) {
    EmilAppLocalizations localizations = EmilAppLocalizations.of(context)!;
    EdgeInsets spacing = getContainerSpacing(context);
    _config ??= context.watch<Configuration>();

    const EdgeInsets contentPadding = EdgeInsets.symmetric(
      horizontal: 5,
      vertical: 2.5,
    );

    return EmilScaffold(
      child: CustomScrollView(
        slivers: <Widget>[
          if (widget.appBar != null) widget.appBar!,
          SliverList(
            delegate: SliverChildListDelegate(
              [
                GlassCard(
                  margin: spacing,
                  borderRadius: 16,
                  backgroundColor: Colors.white.withOpacity(_config!.opacity),
                  children: [
                    getTitle(context, localizations.gameplayTitle),
                    const Divider(),
                    _listTile(
                      child: SwitchListTile(
                        contentPadding: contentPadding,
                        title: _text(localizations.timer, kSecondaryText),
                        value: _config!.isTimerActive,
                        onChanged: (bool value) {
                          setState(() {
                            _config!.setTimerActive = value;
                          });

                          _config!.save();
                        },
                        secondary: const Icon(Icons.timelapse_rounded),
                      ),
                    ),
                    _listTile(
                      child: SwitchListTile(
                        contentPadding: contentPadding,
                        title: _text(localizations.correctMistakes, kSecondaryText),
                        value: _config!.isCorrectMistakesActive,
                        onChanged: (bool value) {
                          setState(() {
                            _config!.setCorrectMistakesActive = value;
                          });

                          _config!.save();
                        },
                        secondary: const Icon(Icons.check_circle_rounded),
                      ),
                    ),
                  ],
                ),
                GlassCard(
                  margin: spacing,
                  borderRadius: 16,
                  backgroundColor: Colors.white.withOpacity(_config!.opacity),
                  children: [
                    getTitle(context, localizations.difficultyTitle),
                    const Divider(),
                    _listTile(
                      child: Column(
                        children: [
                          ..._config!.difficulties.map(
                            (e) => RadioListTile<Difficulty>(
                                title: _text(e.title, kSecondaryText),
                                contentPadding: contentPadding,
                                value: e,
                                groupValue: _config!.selectedDifficulty,
                                onChanged: (val) {
                                  if (val != null) {
                                    setState(() {
                                      _config!.selectedDifficulty = val;
                                    });
                                    _config!.save();
                                  }
                                }),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                GlassCard(
                  margin: spacing,
                  borderRadius: 16,
                  backgroundColor: Colors.white.withOpacity(_config!.opacity),
                  children: [
                    getTitle(context, localizations.playerTitle),
                    const Divider(),
                    SizedBox(
                      width: 192,
                      height: 192,
                      child: AvatarSelect(
                        avatar: _config!.player.avatar.image,
                        onChanged: (filepath) => setState(() {
                          _config!.player.avatar.filepath = filepath;
                          _config!.save();
                        }),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFieldListTile(
                      initialValue: _config!.player.name,
                      hintText: localizations.name,
                      onSaved: (value) => setState(() {
                        _config!.player.name = value ?? '';
                        _config!.save();
                      }),
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: kSecondaryText,
                          ),
                    ),
                  ],
                ),
                GlassCard(
                  margin: spacing,
                  borderRadius: 16,
                  backgroundColor: Colors.white.withOpacity(_config!.opacity),
                  children: [
                    getTitle(context, localizations.effects),
                    const Divider(),
                    _listTile(
                      child: Column(
                        children: [
                          SwitchListTile(
                            title:
                                _text(localizations.animatedBg, kSecondaryText),
                            contentPadding: contentPadding,
                            value: _config!.hasAnimatedBackground,
                            onChanged: (bool value) {
                              setState(() {
                                _config!.setAnimatedBackground = value;
                              });
                              _config!.save();
                            },
                            secondary: const Icon(Icons.animation_rounded),
                          ),
                          SwitchListTile(
                            title:
                                _text(localizations.bgShader, kSecondaryText),
                            contentPadding: contentPadding,
                            value: _config!.isShaderActive,
                            onChanged: (bool value) {
                              setState(() {
                                _config!.setShaderActive = value;
                              });
                              _config!.save();
                            },
                            secondary: const Icon(Icons.filter),
                          ),
                          SwitchListTile(
                            title: _text(
                                localizations.glassMorphism, kSecondaryText),
                            contentPadding: contentPadding,
                            value: _config!.isGlassMorphismActivated,
                            onChanged: (bool value) {
                              setState(() {
                                _config!.setGlassMorphismActivate = value;
                              });
                              _config!.save();
                            },
                            secondary: const Icon(Icons.gradient),
                          ),
                          ListTile(
                            contentPadding: contentPadding,
                            leading: const Icon(Icons.opacity_rounded),
                            title: Row(children: [
                              _text(localizations.opacity, kSecondaryText),
                              Expanded(
                                child: Slider(
                                  value: _config!.opacity,
                                  min: 0.05,
                                  max: 0.25,
                                  onChangeEnd: (_) => _config!.save(),
                                  onChanged: (double val) =>
                                      setState(() => _config!.opacity = val),
                                  label: localizations.opacity,
                                ),
                              ),
                            ]),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                GlassCard(
                  margin: spacing,
                  borderRadius: 16,
                  backgroundColor: Colors.white.withOpacity(_config!.opacity),
                  children: [
                    getTitle(context, localizations.about),
                    const Divider(),
                    Material(
                      type: MaterialType.button,
                      color: Colors.transparent,
                      child: AboutListTile(
                        applicationName: localizations.appShortTitle,
                        applicationVersion: localizations.version,
                        applicationLegalese: localizations.legal,
                        applicationIcon: Image(
                          image: Image.asset('assets/icon.png').image,
                          width: MediaQuery.of(context).size.width * 0.1,
                        ),
                        child: _listTile(
                          child: aboutTile(context),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

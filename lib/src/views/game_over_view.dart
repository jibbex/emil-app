import 'package:emil_app/src/extensions/duration_apis.dart';
import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/models/high_scores/high_scores.dart';
import 'package:emil_app/src/models/high_scores/score.dart';
import 'package:emil_app/src/models/player.dart';
import 'package:emil_app/src/models/tasks.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/widgets/TasksBottomSheet.dart';
import 'package:emil_app/src/widgets/animated_percent_indicator.dart';
import 'package:emil_app/src/widgets/emil_scaffold.dart';
import 'package:emil_app/src/widgets/glass_card.dart';
import 'package:emil_app/src/widgets/input.dart';
import 'package:count_number/count_number.dart';
import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// TODO: Last changes before merging into main branch
class GameOverView extends StatefulWidget {
  final List<Player> players;
  final bool timingMode;
  final SliverAppBar? appBar;

  const GameOverView({
    super.key,
    this.appBar,
    this.timingMode = false,
    required this.players,
  });

  @override
  State<GameOverView> createState() => _GameOverViewState();
}

class _Player extends Player {
  int scoreVal;
  Duration? durationVal;

  _Player({
    required Player player,
    this.scoreVal = 0,
  }) : super(
          id: player.id,
          name: player.name,
          score: player.score,
          startTime: player.startTime,
          difficulty: player.difficulty,
          taskCount: player.taskCount,
          correctCount: player.correctCount,
          timingMode: player.timingMode,
          finished: player.finishedTasks,
          tasks: player.tasks,
        );
}

// TODO: Iterate over players list and refactor using the player objects member rather than states member.
class _GameOverViewState extends State<GameOverView> {
  int _scoreVal = 0;
  late Duration? _duration;
  late final int _correctResults;
  late final int _taskCount;
  late final Tasks _correctTasks;
  late final Score _score;
  late final List<Score> _scores;
  late final List<int> _correctResultList;
  late final List<int> _taskCountList;
  late final ConfettiController _confettiController;
  late final CountNumber _countNum;
  late final DateTime _now;
  late final List<_Player> _players;
  late final List<CountNumber> _countNumbers;

  void calculateScores() {
    for (final player in widget.players) {
      player.score = Score(
        player.tasks.correctTasks,
        player.startTime!.difference(_now),
      );
    }
  }

  @override
  void initState() {
    _now = DateTime.now();
    _players = <_Player>[];
    _countNumbers = <CountNumber>[];
    _scores = <Score>[];

    _correctTasks = widget.players[0].tasks.correctTasks;
    _taskCount = widget.players[0].finishedTasks.isEmpty
        ? 1
        : widget.players[0].finishedTasks.length;
    _correctResults = widget.players[0].correctCount;
    _duration = const Duration(seconds: 0);
    _confettiController = ConfettiController();
    _score = Score(
      _correctTasks,
      widget.players[0].startTime!.difference(DateTime.now()),
    );
    _countNum = CountNumber(
      endValue: widget.timingMode ? _score.score : _score.duration.inSeconds,
      onUpdate: (value) {
        if (widget.timingMode) {
          setState(() => _scoreVal = value as int);
        } else {
          setState(() => _duration = Duration(seconds: value as int));
        }
      },
      onDone: () {
        if (_correctResults / _taskCount > 0.8) {
          _confettiController.play();
        }
      },
    );

    for (final player in widget.players) {
      player.score = Score(
        player.tasks.correctTasks,
        player.startTime!.difference(_now),
      );

      _players.add(_Player(player: player));
      _countNumbers.add(CountNumber(
        endValue: _players.last.timingMode
            ? _players.last.score!.score
            : _players.last.score!.duration.inSeconds,
        onUpdate: (value) {
          if (_players.last.timingMode) {
            setState(() => _players.last.scoreVal = value as int);
          } else {
            setState(
              () => _players.last.durationVal = Duration(seconds: value as int),
            );
          }
        },
        onDone: () {
          if (_players.first.correctCount / _players.first.taskCount > 0.8) {
            _confettiController.play();
          }
        },
      ));
    }

    super.initState();
  }

  @override
  void dispose() {
    for (final countNumber in _countNumbers) {
      countNumber.stop();
    }
    _countNum.stop();
    _confettiController.dispose();
    super.dispose();
  }

  // TODO: Using bottom sheet to set name and avatar, if not configured.
  void _handlePlayerSubmit(String value) async {
    if (value.isEmpty) {
      EmilAppLocalizations localizations = EmilAppLocalizations.of(context)!;

      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  left: 0,
                  top: 2.5,
                  bottom: 2.5,
                  right: 10,
                ),
                child: Icon(
                  Icons.error_rounded,
                  size: 32,
                  color: Colors.black.withOpacity(0.5),
                ),
              ),
              Text(localizations.error),
            ],
          ),
          content: Text('Name ${localizations.notEmpty}.'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: Text(localizations.ok.toUpperCase()),
            ),
          ],
        ),
      );
      return;
    }

    var conf = context.read<Configuration>();
    var hs = context.read<HighScores>();

    if (widget.timingMode) {
      await hs.loadData('time_scores');
    } else {
      await hs.loadData('high_scores');
    }

    final player = Player.fromBasePlayer(conf.player);
    player.difficulty = conf.getIndexOf(conf.selectedDifficulty);
    player.score = _score;
    player.taskCount = _taskCount;
    player.timingMode = conf.isTimerActive;

    hs.add(player);

    if (mounted) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/highscores', (route) => route.isFirst);
      hs.sort((player) => player.score!.duration, false);
    }
  }

  void _showBottomSheet({
    required BuildContext context,
  }) =>
      TasksBottomSheet.show(
        context: context,
        tasks: widget.players[0].tasks,
        count: widget.players[0].taskCount,
      );

  double _getCardHeight(double height) {
    if (height > 920) {
      return height * 0.5;
    } else if (height > 820) {
      return height * 0.55;
    } else if (height > 760) {
      return height * 0.60;
    } else if (height > 680) {
      return height * 0.65;
    } else if (height > 620) {
      return height * 0.70;
    } else if (height > 580) {
      return height;
    }

    return 550;
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double opacity = context.read<Configuration>().opacity;
    EmilAppLocalizations localizations = EmilAppLocalizations.of(context)!;

    return EmilScaffold(
      child: Stack(
        children: [
          CustomScrollView(
            slivers: <Widget>[
              if (widget.appBar != null) widget.appBar!,
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Container(
                      margin: getContainerSpacing(context),
                      constraints: BoxConstraints(
                        maxWidth: 400,
                        maxHeight: height > 550 ? height - 100 : 450,
                      ),
                      child: Align(
                        heightFactor: 2,
                        alignment: Alignment.center,
                        child: GlassCard(
                          height: _getCardHeight(height),
                          borderRadius: 16,
                          backgroundColor: Colors.white.withOpacity(opacity),
                          children: [
                            getTitle(context,
                                '$_correctResults ${localizations.getOf} $_taskCount ${localizations.correct}'),
                            const Divider(),
                            SizedBox(
                              height: height < 475 ? 10 : 15,
                            ),
                            AnimatedPercentIndicator(
                              value: _correctResults == 0
                                  ? 0
                                  : _correctResults / _taskCount,
                              onDone: () => _countNum.start(),
                              isAnimated: true,
                              onPressed: () =>
                                  _showBottomSheet(context: context),
                              textStyle: eqTextStyle(context).copyWith(
                                fontSize: 34,
                                color: const Color(0x88ffffff),
                              ),
                            ),
                            SizedBox(
                              height: height < 475 ? 15 : 30,
                            ),
                            if (widget.timingMode)
                              Text(
                                '${_scoreVal.toStringAsFixed(0)} ${localizations.scores}',
                                style: eqTextStyle(context).copyWith(
                                  fontSize: 22,
                                  color: const Color(0x88ffffff),
                                ),
                              ),
                            if (_duration != null && !widget.timingMode)
                              Text(
                                _duration!.formattedString,
                                style: eqTextStyle(context).copyWith(
                                  fontSize: 22,
                                  color: const Color(0x88ffffff),
                                ),
                              ),
                            const SizedBox(
                              height: 15,
                            ),
                            Input(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 15,
                                vertical: 5,
                              ),
                              hintText: localizations.name,
                              onSubmitted: _handlePlayerSubmit,
                              maxLength: 30,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.topCenter,
            child: ConfettiWidget(
              confettiController: _confettiController,
              blastDirectionality: BlastDirectionality.explosive,
              emissionFrequency: 0.1,
              numberOfParticles: 12,
              shouldLoop: true,
              colors: const [
                Colors.green,
                Colors.blue,
                Colors.pink,
                Colors.orange,
                Colors.purple
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/views/calculation_view.dart';
import 'package:emil_app/src/widgets/app_bar.dart';
import 'package:emil_app/src/widgets/app_bar_button.dart';
import 'package:emil_app/src/widgets/emil_scaffold.dart';
import 'package:emil_app/src/widgets/glass_button.dart';
import 'package:emil_app/src/widgets/glass_card.dart';
import 'package:emil_app/src/widgets/qr.dart';
import 'package:emil_app/src/ws_client.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';

class MultiplayerView extends StatefulWidget {
  final SliverAppBar? appBar;
  final String baseUrl;

  const MultiplayerView({
    super.key,
    this.appBar,
    this.baseUrl = 'ws://localhost:8080/socket',
  });

  @override
  State<MultiplayerView> createState() => _MultiplayerViewState();
}

class _MultiplayerViewState extends State<MultiplayerView>
    with TickerProviderStateMixin {
  late final AnimationController _countDownAnimationController;
  late final Animation<int> _countDownAnimation; // TODO: implement countdown
  late final WsClient _client;
  bool ready = false;
  bool start = false;
  int? latency;
  Configuration? _config;

  void _onConnectionError(Object error) {
    String code = "0";
    const kEmpty = '';
    final localizations = EmilAppLocalizations.of(context)!;
    final errMsg = error.toString();
    final matches = RegExp(r'errno = (\d+)').allMatches(errMsg);
    final message = errMsg
        .replaceAll(RegExp(r'.+Exception: '), kEmpty)
        .replaceAll(RegExp(r'\(.+\).+', multiLine: true, dotAll: true), kEmpty);

    try {
      if (matches.first.groupCount == 1) {
        code = matches.first.group(1)!;
      }
    } catch (error) {
      if (kDebugMode) {
        rethrow;
      }
    }

    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 0,
                top: 2.5,
                bottom: 2.5,
                right: 10,
              ),
              child: Icon(
                Icons.error_rounded,
                size: 32,
                color: Colors.black.withOpacity(0.5),
              ),
            ),
            Expanded(
              child: Text('${localizations.error}: $code'),
            ),
          ],
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              localizations.connectionError,
              textAlign: TextAlign.left,
              style: Theme.of(context).textTheme.headline3!.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.italic,
                    color: kPrimaryText.withOpacity(0.85),
                  ),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(message),
          ],
        ),
        actions: <Widget>[
          TextButton(
            onPressed: _reconnect,
            child: Text(localizations.retry.toUpperCase()),
          ),
          TextButton(
            onPressed: _popBack,
            child: Text(localizations.ok.toUpperCase()),
          ),
        ],
      ),
    );
  }

  void _popBack() {
    Navigator.pop(context);
    Navigator.pop(context);
  }

  void _reconnect() {
    Navigator.pop(context);
    _connect();
  }

  void _onData(Command cmd) {
    if (kDebugMode) print(cmd.payload);
    if (cmd.type == CommandType.ready) {
      setState(() => ready = true);
    }
  }

  void _connect([String? channel]) {
    _client = WsClient.connect(widget.baseUrl, channel);
    _client.listen(_onData, onError: _onConnectionError);
  }

  void _onStartPressed() {
    setState(() => start = true);

    _client.send(
      Command(CommandType.goto, {
        'view': 'CalculationView',
      }),
    );

    _client.channel.then(
      (channel) => Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) {
            EmilAppLocalizations localizations =
                EmilAppLocalizations.of(context)!;
            double width = MediaQuery.of(context).size.width;

            return CalculationView(
              isMultiplayerSession: true,
              channel: channel,
              baseUrl: widget.baseUrl,
              peerType: PeerType.primary,
              appBar: appBar(
                context: context,
                title: width > 960
                    ? localizations.appTitle
                    : localizations.appShortTitle,
                leading: AppBarButton(
                  const Icon(Icons.arrow_back_ios_new),
                  onPressed: () => Navigator.of(context).pop(),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _futureBuilder(BuildContext context, AsyncSnapshot<String> snapshot) {
    final size = MediaQuery.of(context).size;

    if (snapshot.hasError) {
      _onConnectionError(snapshot.error!);
    }

    if (!snapshot.hasData) {
      return SizedBox(
        height: 128,
        width: 128,
        child: CircularProgressIndicator(
          backgroundColor: Colors.transparent,
          strokeWidth: 14,
          color: kPrimary.withOpacity(0.25),
        ),
      );
    }

    return Qr(
      data: snapshot.data!,
      active: ready,
      size: size.height < size.width ? size.height * 0.5 : size.width * 0.85,
      borderRadius: BorderRadius.all(
        Radius.circular(size.width < 860 ? 0 : 16),
      ),
      padding: const EdgeInsets.all(16),
      foregroundColor: const Color(0x45000000),
      child: GlassButton(
        width: 250,
        height: 85,
        glass: _config!.isGlassMorphismActivated,
        onPressed: _onStartPressed,
        child: Center(
          child: Text(
            EmilAppLocalizations.of(context)!.start,
            style: gmBtnTextStyle,
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    if (!start) _connect();
    _countDownAnimationController = AnimationController(
      value: 5,
      duration: const Duration(seconds: 5),
      vsync: this,
    );
    _countDownAnimation = IntTween(
      begin: 5,
      end: 0,
    ).animate(_countDownAnimationController);
    super.initState();
  }

  @override
  void dispose() {
    _client.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final localizations = EmilAppLocalizations.of(context)!;
    final spacing = getContainerSpacing(context);
    _config ??= context.watch<Configuration>();
    String title = localizations.multiplayer;

    return EmilScaffold(
      child: CustomScrollView(
        slivers: <Widget>[
          if (widget.appBar != null)
            appBar(
                context: context,
                title: title,
                leading: widget.appBar!.leading,
                actions: [
                  Padding(
                    padding: const EdgeInsets.all(5),
                    child: AppBarButton(
                      const Icon(Icons.share_rounded),
                      onPressed: () async => Share.share(
                        'https://emil.michm.de/#/channel/${await _client.channel}',
                        subject: localizations.invite,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                  ),
                  ...?widget.appBar!.actions,
                ]),
          SliverList(
            delegate: SliverChildListDelegate([
              SizedBox(
                height: size.height - 100,
                width: size.width,
                child: Column(
                  children: [
                    const Spacer(),
                    GlassCard(
                      height: (size.width < 860 ? 0.65 : 0.75) * size.height,
                      margin: spacing,
                      borderRadius: 16,
                      backgroundColor:
                          Colors.white.withOpacity(_config!.opacity),
                      children: [
                        getTitle(context, localizations.multiplayerInfo),
                        const Divider(),
                        const Spacer(),
                        Center(
                          child: FutureBuilder<String>(
                            future: _client.id,
                            builder: _futureBuilder,
                          ),
                        ),
                        const Spacer(),
                      ],
                    ),
                    const Spacer(),
                  ],
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

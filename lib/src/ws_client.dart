import 'dart:async';
import 'dart:convert';
import 'package:uuid/uuid.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

/// To indicate first and second player.
enum PeerType { primary, secondary }

/// Enum of available commands, that can be send and received.
enum CommandType {
  sync,
  ping,
  pong,
  ack,
  data,
  ready,
  goto,
  req,
  task,
  player,
  end,
  start,
  done,
  gameOver;

  /// Tries to find the Commands value of the passed String.
  static CommandType? valueOf(String name) {
    for (var cmd in CommandType.values) {
      if (cmd.name == name) {
        return cmd;
      }
    }

    return null;
  }
}

/// [Command] class of commands that can be interpreted and generated
/// from [WsClient] object.
class Command {
  final CommandType type;
  Map<String, dynamic>? payload;

  Command(this.type, [this.payload]);

  factory Command.fromMap(Map<String, dynamic> map) {
    assert(map['cmd'] != null);
    return Command(
      CommandType.valueOf(map.remove(map['cmd']) ?? 'data')!,
      map.isEmpty ? null : map,
    );
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = {'cmd': type.name};

    if (payload != null) {
      map.addAll(payload!);
    }

    return map;
  }

  void append(MapEntry<String, dynamic> entry) {
    payload ??= {};
    payload!.putIfAbsent(entry.key, () => entry.value);
  }
}

/// WebSocket client that connects to relay server
class WsClient {
  /// First and second peer sometimes have to react
  /// differently on an incoming [Command].
  late PeerType _type;

  /// Random [id] of the [WsClient] instance.
  late String _id;

  /// Returns the client [id] after successful connected.
  final Completer<String> _idCompleter = Completer();

  final Completer<String> _channelCompleter = Completer();

  /// Base WebSocket URL
  final String baseUrl;

  /// Decoded message [stream]
  late StreamController<Command> _streamController;

  /// WebSocket [sink]
  late WebSocketSink _sink;

  /// Called on error
  void Function(Object error)? _onError;

  /// Called on non internal needed messages
  void Function(Command cmd)? _onMsg;

  /// Time of first received message
  DateTime? _start;

  /// Calculated latency for send -> receive
  int? _latency;

  /// Stream sub
  StreamSubscription? _sub;

  /// WebSocket URL
  String? _url;

  /// Incoming [Tasks]
  int tasksLength = 0;

  /// Standard constructor.
  WsClient(this.baseUrl) : _id = const Uuid().v4();

  /// Private factory for initialization of an [WsClient] object.
  factory WsClient._init({
    String? channel,
    String? id,
    PeerType? type,
    Function(Command cmd)? onMsg,
    Function(Object error)? onError,
    required String baseUrl,
  }) {
    final client = WsClient(baseUrl);
    client._id = id ?? const Uuid().v4();
    client._type = type ?? PeerType.secondary;
    client._url = '$baseUrl/';
    client._url =
        channel != null ? client._url! + channel : client._url! + client._id;

    final ws = WebSocketChannel.connect(Uri.parse(client._url!))
      ..stream
          .map((msg) => client._decode(msg))
          .listen(client._onData, onError: onError);

    client._sink = ws.sink;
    client._streamController = StreamController();

    if (onMsg != null) {
      client._onMsg = onMsg;
      client._sub = client._streamController.stream.listen(client._onMsg);
    }

    return client;
  }

  factory WsClient.connect(String baseUrl, [String? channel]) {
    return WsClient._init(
      baseUrl: baseUrl,
      channel: channel,
    );
  }

  int get latency => _latency ?? -1;

  Future<String> get id => _idCompleter.future;

  Stream<Command> get stream => _streamController.stream;

  PeerType get type => _type;

  Future<String> get channel => _channelCompleter.future;

  /// Connect to WebSocket Relay Server
  void connect([String? channel]) {
    _type = PeerType.secondary;
    _url = '$baseUrl/';
    _url = channel != null ? _url! + channel : _url! + _id;

    final ws = WebSocketChannel.connect(Uri.parse(_url!))
      ..stream.map((msg) => _decode(msg)).listen(_onData, onError: _onError);

    _sink = ws.sink;
    _streamController = StreamController();

    if (_onMsg != null) {
      _sub = _streamController.stream.listen(_onMsg);
    }
  }

  /// Internal web socket [stream] listener.
  void _onData(Map<String, dynamic> msg) {
    if (msg.containsKey('ok')) {
      if (msg['ok'] == 1) {
        if (msg.containsKey('cmd') && msg['cmd'] != null) {
          switch (msg['cmd']) {
            case CommandType.sync:
              _type = PeerType.primary;
              send(CommandType.ping);
              break;

            case CommandType.ping:
              send(CommandType.pong);
              break;

            case CommandType.pong:
              if (_latency == null && msg.containsKey('latency')) {
                _latency = msg['latency'];
              }

              send(CommandType.ack);
              _streamController.sink.add(Command(CommandType.ready));
              break;

            case CommandType.ready:
              _streamController.sink.add(Command(CommandType.ready));
              break;

            case CommandType.goto:
              _streamController.sink.add(Command(CommandType.goto, {
                'view': msg['view'],
              }));
              break;

            case CommandType.task:
              if (msg.containsKey('length')) {
                tasksLength = msg['length'];
                _streamController.sink.add(Command(CommandType.task, {
                  'length': msg['length'],
                }));
              } else if (msg.containsKey('index')) {
                _streamController.sink.add(Command(CommandType.task, {
                  'task': msg['task'],
                }));
                if (msg['index'] == tasksLength - 1) {
                  _streamController.sink.add(Command(CommandType.ack));
                }
              }
              break;

            case CommandType.req:
              _streamController.sink.add(Command(CommandType.req));
              break;

            case CommandType.done:
              if (msg['id'] != _id) {
                _streamController.sink.add(Command(CommandType.done, {
                  'task': msg['task'],
                }));
              }
              break;

            case CommandType.gameOver:
              if (msg['id'] != _id) {
                _streamController.sink.add(Command(CommandType.gameOver, {
                  'player': {
                    'tasks': msg['tasks'],
                    'correct': msg['correct'],
                    'count': msg['count'],
                    'startTime': msg['startTime'],
                    'avatarImage': msg['avatarImage'],
                  },
                }));
              }
              break;

            case CommandType.player:
              if (msg['id'] != _id) {
                _streamController.sink.add(Command(CommandType.player, msg));
              }
              break;

            case CommandType.start:
              _streamController.sink.add(Command(CommandType.start));
              break;

            case CommandType.end:
              _streamController.sink.add(Command(CommandType.end));
              break;

            default:
              msg.putIfAbsent('cmd', () => 'data');
              _streamController.sink.add(Command.fromMap(msg));
          }
        } else {
          if (!_idCompleter.isCompleted) {
            _idCompleter.complete(_id);
          }

          if (!_channelCompleter.isCompleted) {
            _channelCompleter.complete(msg['channel']);
          }
        }
      } else if (msg['ok'] == 0) {
        final error = Exception(
          msg.containsKey('error') ? msg['error']['msg'] : null,
        );

        _idCompleter.completeError(error);
        if (_onError != null) _onError!(error);
      }
    }
  }

  /// Decodes an json [String] to a [Map].
  Map<String, dynamic> _decode(String msg) {
    Map<String, dynamic> json = jsonDecode(msg);

    if (json.containsKey('cmd')) {
      json['cmd'] = CommandType.valueOf(json['cmd']);

      if (_start != null && json.containsKey('time')) {
        _latency = _start!.millisecond - json['time'] as int;
      }
    }

    return json;
  }

  /// Sends a encoded command to all peers. The passed parameter cmd must be
  /// either of type [CommandType] or [Command]. Adds status code and client
  /// [id] to each message. The server needs the ID so that the message is not
  /// echoed back. If the command is of type [CommandType.sync] or
  /// [CommandType.ping], the current timestamp is added to the message.
  void send(dynamic cmd) {
    assert(cmd is CommandType || cmd is Command);
    late final CommandType type;
    late final Command command;

    if (cmd is CommandType) {
      type = cmd;
      command = Command(cmd);
    } else if (cmd is Command) {
      type = cmd.type;
      command = cmd;
    }

    final map = command.toMap()..addAll({'ok': 1, 'id': _id});

    if (type == CommandType.sync || type == CommandType.ping) {
      _start = DateTime.now();
      map.update(
        'time',
        (value) => _start!.millisecond,
        ifAbsent: () => _start!.millisecond,
      );
    }

    _sink.add(jsonEncode(map));
  }

  /// Listens to the command stream
  Future<void> listen(
    void Function(Command) onData, {
    void Function(Object)? onError,
    void Function()? onDone,
    bool? cancelOnError,
  }) async {
    await cancel();
    _onMsg = onData;
    _onError = onError;
    _sub = _streamController.stream.listen(
      _onMsg,
      onError: _onError,
      onDone: onDone,
    );
  }

  Future<void> cancel() async {
    await _sub?.cancel();
  }

  /// Cancels the subscription and closes the sink of the socket.
  void close() {
    cancel();
    _sink.close();
  }
}

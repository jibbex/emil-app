import 'package:emil_app/src/models/high_scores/high_scores.dart';
import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:flutter/material.dart';

class ScoreDataTable extends StatefulWidget {
  final HighScores scores;

  const ScoreDataTable({
    super.key,
    required this.scores,
  });

  @override
  State<ScoreDataTable> createState() => _ScoreDataTableState();
}

class _ScoreDataTableState extends State<ScoreDataTable> {

  DataColumn colHeader(String title) => DataColumn(label: getTitle(context, title));

  @override
  Widget build(BuildContext context) {
    EmilAppLocalizations localizations = EmilAppLocalizations.of(context)!;

    return Material(
      type: MaterialType.transparency,
      color: Colors.transparent,
      surfaceTintColor: Colors.transparent,
      child: Flex(
        direction: Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: PaginatedDataTable(
              source: widget.scores,
              columns: <DataColumn>[
                colHeader(localizations.name),
                colHeader(localizations.scores),
                colHeader(localizations.time),
                colHeader(localizations.tasks),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
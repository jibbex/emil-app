import 'package:emil_app/src/models/configuration.dart';
import 'package:flutter/material.dart';
import 'package:glass_kit/glass_kit.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:provider/provider.dart';

class NumButton extends StatelessWidget {
  final bool? hasExtraWidth;
  final bool? hasExtraHeight;
  final String value;
  final Widget? icon;
  final void Function(String)? onPressed;

  const NumButton({
    super.key,
    required this.value,
    required this.onPressed,
    this.hasExtraHeight,
    this.hasExtraWidth,
    this.icon,
  });

  void _onPressed() {
    if (onPressed != null) {
      onPressed!(value);
    }
  }

  @override
  Widget build(BuildContext context) =>
      LayoutBuilder(builder: (context, constraints) {
        double opacity = context.read<Configuration>().opacity;
        return GlassContainer(
          height: constraints.maxHeight,
          width: constraints.maxWidth,
          gradient: LinearGradient(
            colors: [
              Colors.white.withOpacity(opacity + 0.05),
              Colors.white.withOpacity(opacity)
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderGradient: !kIsWeb
              ? LinearGradient(
                  colors: [
                    Colors.white.withOpacity(opacity + 0.15),
                    Colors.white.withOpacity(opacity + 0.05),
                    Colors.lightBlueAccent.withOpacity(opacity),
                    Colors.lightBlueAccent.withOpacity(opacity + 0.25)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: const [0.0, 0.39, 0.40, 1.0],
                )
              : null,
          borderColor: kIsWeb ? Colors.white.withOpacity(opacity + 0.1) : null,
          blur: 10.0,
          borderWidth: kIsWeb ? 2 : 3,
          elevation: 12.0,
          isFrostedGlass: context.watch<Configuration>().isGlassMorphismActivated,
          shadowColor: Colors.black.withOpacity(opacity + 0.2),
          borderRadius: const BorderRadius.all(Radius.circular(20)),
          alignment: Alignment.center,
          frostedOpacity: opacity < 0.2 ? opacity : 0.2,
          margin: const EdgeInsets.all(2.5),
          padding: const EdgeInsets.all(0),
          child: Material(
            color: Colors.white.withOpacity(0.0),
            child: InkWell(
              splashColor: kPrimary.withOpacity(0.2),
              hoverColor: kLightBackground.withOpacity(0.05),
              highlightColor: kPrimary.withOpacity(0.05),
              onTap: _onPressed,
              child: Center(
                child: icon ??
                    Text(
                      value,
                      style: gmBtnTextStyle,
                    ),
              ),
            ),
          ),
        );
      });
}

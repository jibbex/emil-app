import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class BackgroundWaves extends StatelessWidget {
  final Widget child;
  final Widget? backgroundChild;
  final CustomConfig _customConfig;
  final Color? backgroundColor;
  final double waveAmplitude;
  final double waveFrequency;
  final int? duration;
  final Size size;
  final bool isAnimated;

  BackgroundWaves({
    super.key,
    required this.child,
    this.backgroundColor,
    this.backgroundChild,
    this.waveAmplitude = 6,
    this.size = const Size(double.infinity, double.infinity),
    this.duration = 3600,
    this.waveFrequency = 2,
    bool? isAnimated,
    CustomConfig? config,
  })  : isAnimated = isAnimated ?? true,
        _customConfig = config ??
            CustomConfig(
              gradients: [
                [
                  Colors.red.shade900.withOpacity(0.25),
                  Colors.redAccent.shade700.withOpacity(0.4),
                  kPrimary.withOpacity(0.2),
                ],
                [
                  const Color(0xffd9273c),
                  Colors.redAccent.shade400.withOpacity(0.4),
                  Colors.deepOrangeAccent.shade400.withOpacity(0.4),
                ],
                [
                  Colors.red.shade700.withOpacity(0.6),
                  Colors.red.shade500.withOpacity(0.5),
                  const Color(0x65df1d5c),
                ],
              ],
              durations: const [
                10200,
                9200,
                10600,
              ],
              heightPercentages: [
                0.35,
                0.42,
                0.43,
              ],
            );

  @override
  Widget build(BuildContext context) {
    final config = context.watch<Configuration>();
    Widget shaderBuilder({required Widget child}) => config.isShaderActive
        ? ShaderMask(
            shaderCallback: (Rect bounds) =>
                kBackgroundGradient.createShader(bounds),
            blendMode: BlendMode.lighten,
            child: child,
          )
        : child;

    return Stack(
      children: [
        Transform.rotate(
          angle: -1.3,
          child: Transform.scale(
            scale: 2.2,
            child: shaderBuilder(
              child: WaveWidget(
                config: _customConfig,
                size: size,
                backgroundColor: kBackground.withOpacity(1),
                waveAmplitude: waveAmplitude,
                waveFrequency: waveFrequency,
                heightPercentage: 15.8,
                wavePhase: 85,
                duration: config.hasAnimatedBackground ? duration : 0,
                isLoop: config.hasAnimatedBackground,
              ),
            ),
          ),
        ),
        if (backgroundChild != null) backgroundChild!,
        child,
      ],
    );
  }
}

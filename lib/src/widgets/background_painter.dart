import 'package:emil_app/src/theme/colors.dart';
import 'package:flutter/material.dart';

class BackgroundPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final height = size.height;
    final width = size.width;

    final paint = Paint()
      ..style = PaintingStyle.fill
      ..isAntiAlias = true
      ..color = kPrimary;

    final curvePath = Path()
      ..moveTo(0, height) //4
      ..lineTo(width, height) //6
      ..lineTo(width, 0) //7

      ..quadraticBezierTo(width * 0.25, height * 0.25,
          width * 0.15, height * 0.1) //8/8
      ..quadraticBezierTo(width * 0.6, height * 0.85,
          width * 0.75, height * 0.95) //8
      ..quadraticBezierTo(width * 0.25, height * 0.75,
          width * 0.35, height * 0.5) //8

      ..close();


    canvas.drawPath(curvePath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

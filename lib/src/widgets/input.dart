import 'dart:core';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/widgets/glass_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Input extends StatefulWidget {
  final void Function(String) onSubmitted;
  final String? hintText;
  final EdgeInsets? padding;
  final int? maxLength;

  const Input({
    super.key,
    this.hintText,
    this.padding,
    this.maxLength,
    required this.onSubmitted,
  });

  @override
  State<Input> createState() => _InputState();
}

class _InputState extends State<Input> {
  late final TextEditingController _controller;
  String? name;

  @override
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (name == null) {
      setState(() {
        name = context.read<Configuration>().player.name;
        _controller.text = name!;
      });
    }

    return Padding(
      padding: widget.padding ??
          const EdgeInsets.symmetric(
            horizontal: 15,
            vertical: 5,
          ),
      child: Row(
        children: [
          Flexible(
            child: TextField(
              decoration: InputDecoration(
                hintText: widget.hintText,
              ),
              controller: _controller,
              onSubmitted: widget.onSubmitted,
              maxLength: widget.maxLength,
            ),
          ),
          const SizedBox(
            width: 15,
          ),
          Align(
            alignment: Alignment.topCenter,
            heightFactor: 1.5,
            child: GlassButton(
              width: 60,
              height: 60,
              margin: const EdgeInsets.all(0),
              borderRadius: 8,
              child: Icon(
                Icons.save_rounded,
                size: 42,
                color: kOnPrimary.withOpacity(0.5),
              ),
              onPressed: () => widget.onSubmitted(_controller.value.text),
            ),
          ),
        ],
      ),
    );
  }
}

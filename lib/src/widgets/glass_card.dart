import 'dart:ui';
import 'package:emil_app/src/theme/colors.dart';
import 'package:flutter/material.dart';

class GlassCard extends StatelessWidget {
  final List<BoxShadow> _boxShadow;
  final double? borderRadius;
  final double blur;
  final double? height;
  final double? width;
  final double borderWidth;
  final List<Widget>? children;
  final Widget? child;
  final EdgeInsets? margin;
  final Color backgroundColor;
  final Color borderColor;
  final Gradient? gradient;

  GlassCard({
    super.key,
    this.height,
    this.width,
    this.child,
    this.children,
    this.margin,
    this.gradient,
    this.blur = 20,
    this.borderRadius,
    this.borderWidth = 1.5,
    this.backgroundColor = const Color(0x0dffffff),
    this.borderColor = const Color(0x1affffff),
    List<BoxShadow>? boxShadow,
  }) : _boxShadow = boxShadow ??
            <BoxShadow>[
              const BoxShadow(
                  blurRadius: 6,
                  spreadRadius: 0,
                  color: kShadow,
                  blurStyle: BlurStyle.outer),
            ] {
    assert(
      children == null && child != null || children != null && child == null,
      "Please pass only children or child property",
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: margin,
      decoration: BoxDecoration(
        boxShadow: _boxShadow,
        borderRadius: BorderRadius.circular(borderRadius ?? 0),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(borderRadius ?? 0),
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: blur,
            sigmaY: blur,
          ),
          child: Container(
            decoration: BoxDecoration(
              gradient: gradient,
              color: gradient == null ? backgroundColor : Colors.transparent,
              borderRadius: borderRadius != null
                  ? BorderRadius.circular(borderRadius!)
                  : null,
              border: borderWidth > 0
                  ? Border.all(
                      width: borderWidth,
                      color: borderColor,
                    )
                  : null,
            ),
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Column(
                children: children ?? [if (child != null) child!],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

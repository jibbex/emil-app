import 'package:count_number/count_number.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class AnimatedPercentIndicator extends StatefulWidget {
  final bool isAnimated;
  final double value;
  final double? velocity;
  final double? startValue;
  final TextStyle? textStyle;
  final SpringDescription? springDescription;
  final void Function()? onDone;
  final void Function()? onPressed;

  const AnimatedPercentIndicator({
    super.key,
    this.onDone,
    this.velocity,
    this.startValue,
    this.textStyle,
    this.onPressed,
    this.springDescription,
    this.isAnimated = false,
    required this.value,
  });

  @override
  State<AnimatedPercentIndicator> createState() =>
      _AnimatedPercentIndicatorState();
}

class _AnimatedPercentIndicatorState extends State<AnimatedPercentIndicator> {
  late final CountNumber _countNum;
  double _value = 0;

  @override
  void initState() {
    _countNum = CountNumber(
      startValue: widget.startValue,
      endValue: widget.value,
      velocity: widget.velocity,
      springDescription: widget.springDescription,
      onDone: widget.onDone,
      onUpdate: (value) => setState(() => _value = value as double),
    );

    _countNum.start();
    super.initState();
  }

  @override
  void dispose() {
    _countNum.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Material(
          type: MaterialType.circle,
          color: Colors.transparent,
          clipBehavior: Clip.antiAlias,
          child: InkWell(
            onTap: widget.onPressed,
            child: CircularPercentIndicator(
              circularStrokeCap: CircularStrokeCap.round,
              backgroundColor: Colors.transparent,
              startAngle: 360,
              center: Icon(
                Icons.check_rounded,
                size: 100,
                color: Colors.white.withOpacity(0.2),
              ),
              radius: 70,
              percent: _value,
              lineWidth: 14,
              progressColor: kPrimary.withOpacity(0.25),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: height < 420 ? 7.5 : 15),
          child: Text(
            '${(_value * 100).toStringAsFixed(0)} %',
            style: widget.textStyle,
          ),
        ),
      ],
    );
  }
}

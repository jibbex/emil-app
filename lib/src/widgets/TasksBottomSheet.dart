import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/models/task.dart';
import 'package:emil_app/src/models/tasks.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TasksBottomSheet {
  final Tasks tasks;
  final BuildContext context;
  final int count;

  const TasksBottomSheet(this.context, this.tasks, this.count);

  Widget _tileBuilder(_, index) => _TaskListTile(tasks[index]);
  Widget _builder(_) {
    var conf = context.read<Configuration>();
    var pattern = conf.selectedDifficulty.getPatterns[0];
    var protoTask = Task(pattern);
    protoTask.value = protoTask.result;

    return ListView.builder(
      itemCount: tasks.length,
      prototypeItem: _TaskListTile(protoTask),
      itemBuilder: _tileBuilder,
    );
  }

  static void show({
    required BuildContext context,
    required Tasks tasks,
    required int count,
  }) {
    var sheet = TasksBottomSheet(context, tasks, count);
    var size = MediaQuery.of(context).size;

    showModalBottomSheet(
      context: context,
      constraints: BoxConstraints(
        maxHeight: size.height,
        maxWidth: 450,
      ),
      barrierColor: Colors.black26,
      elevation: 16,
      builder: sheet._builder,
    );
  }
}

class _TaskListTile extends StatelessWidget {
  final Task task;

  const _TaskListTile(this.task);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(
        vertical: 2.5,
        horizontal: 10,
      ),
      leading: Icon(
        task.value == task.result ? Icons.check_circle : Icons.error_rounded,
        size: 32,
        color: task.value == task.result ? kPrimary : kError.withOpacity(0.75),
        shadows: [
          Shadow(
            blurRadius: 15,
            color: kLightBackground.withOpacity(0.35),
            offset: Offset.fromDirection(1.1, 2.75),
          )
        ],
      ),
      title: Row(
        children: [
          Expanded(
            flex: 2,
            child: Text(
              '${task.toString()} = ${task.result.toString()}',
              textAlign: TextAlign.right,
              style: eqTextStyle(context).copyWith(
                fontSize: 20,
                color: kPrimary,
                shadows: [
                  Shadow(
                    blurRadius: 15,
                    color: kLightBackground.withOpacity(0.35),
                    offset: Offset.fromDirection(1.1, 2.75),
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            width: 15,
          ),
        ],
      ),
      trailing: SizedBox(
        height: 64,
        width: 50,
        child: Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Text(
            task.value.toString(),
            textAlign: TextAlign.right,
            style: eqTextStyle(context).copyWith(
              fontSize: 32,
              color: kPrimary.withOpacity(0.8),
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.italic,
            ),
          ),
        ),
      ),
    );
  }
}

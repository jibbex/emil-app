import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/widgets/num_button.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class NumPad extends StatefulWidget {
  final Function(String, Function?)? onChanged;
  final String equation;

  const NumPad({
    super.key,
    required this.onChanged,
    required this.equation,
  });

  @override
  State<StatefulWidget> createState() => _NumPadState();
}

class _NumPadState extends State<NumPad> {
  late String _result;

  void _onPressed(String value) {
    if (kDebugMode) {
      print('Key: $value');
    }

    try {
      if (value == '←' && _result.isNotEmpty) {
        if (_result.length <= 1 && _result != '0') {
          setState(() => _result = '0');
        } else {
          setState(() => _result =
              _result != '0' ? _result.substring(0, _result.length - 1) : '0');
        }
      } else if (value == '±') {
        setState(() => _result = (int.parse(_result) / -1).round().toString());
      } else if (_result != '0') {
        setState(() => _result += value);
      } else if (RegExp(r'^-?[\d]+$').firstMatch(value) != null) {
        if (kDebugMode) {
          print(RegExp(r'^-?[\d]+$').firstMatch(value));
        }
        setState(() => _result = value);
      }
    } on Exception catch (_, e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }

  void _resetResult() {
    if (mounted) {
      setState(() => _result = '0');
    }
  }

  void _finishTask() {
    widget.onChanged!(_result, () => _resetResult());
  }

  @override
  void initState() {
    super.initState();
    _result = '0';
  }

  @override
  Widget build(BuildContext context) {
    double deviceHeight = MediaQuery.of(context).size.height;

    TextStyle eqTextStyle = Theme.of(context).textTheme.headline2!.copyWith(
      color: Colors.white.withOpacity(0.4),
      leadingDistribution: TextLeadingDistribution.even,
      fontWeight: FontWeight.w100,
      fontSize: 60,
      decorationColor: kPrimary,
      shadows: [
        Shadow(
          color: Colors.black.withOpacity(0.25),
          offset: Offset.fromDirection(1.1, 2.75),
        )
      ],
    );

    Widget numPad = Center(
      child: SizedBox(
        width: deviceHeight * 0.6,
        child: StaggeredGrid.count(
          crossAxisCount: 4,
          mainAxisSpacing: 4,
          crossAxisSpacing: 4,
          children: [
            StaggeredGridTile.count(
              crossAxisCellCount: 4,
              mainAxisCellCount: 1,
              child:
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.equation,
                    textAlign: TextAlign.justify,
                    strutStyle: StrutStyle.fromTextStyle(
                        Theme.of(context).textTheme.labelLarge!),
                    style: eqTextStyle,
                  ),
                  Text(
                    ' = ',
                    textAlign: TextAlign.center,
                    strutStyle: StrutStyle.fromTextStyle(
                        Theme.of(context).textTheme.labelLarge!),
                    style: eqTextStyle,
                  ),
                  Text(
                    _result,
                    textAlign: TextAlign.center,
                    strutStyle: StrutStyle.fromTextStyle(
                        Theme.of(context).textTheme.labelLarge!),
                    style: eqTextStyle,
                  ),
                ],
              ),),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '7', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '8', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '9', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 2,
              child: NumButton(
                  value: '←',
                  hasExtraHeight: true,
                  onPressed: _onPressed,
                  icon: const Icon(
                    Icons.undo,
                    color: kGmBtnTextColor,
                    size: kGmBtnFontSize,
                  )),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '4', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '5', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '6', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '1', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '2', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '3', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 2,
              child: NumButton(hasExtraHeight: true, value: '=', onPressed: (_) => _finishTask()),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 2,
              mainAxisCellCount: 1,
              child: NumButton(hasExtraWidth: true, value: '0', onPressed: _onPressed),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 1,
              child: NumButton(value: '±', onPressed: _onPressed),
            ),
          ],
        ),
      ),
    );

    return numPad;
  }
}

import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/widgets/background_waves.dart';
import 'package:blur_bottom_bar/blur_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EmilScaffold extends StatelessWidget {
  final Widget child;
  final Widget? backgroundChild;
  final BlurBottomView? bottomNavBar;

  const EmilScaffold({
    super.key,
    required this.child,
    this.backgroundChild,
    this.bottomNavBar,
  });

  @override
  Widget build(BuildContext context) {
    final config = context.watch<Configuration>();

    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: false,
        bottom: false,
        child: DecoratedBox(
          position: DecorationPosition.background,
          decoration: BoxDecoration(
            gradient: kBackgroundGradient,
          ),
          child: BackgroundWaves(
            isAnimated: config.hasAnimatedBackground,
            backgroundChild: backgroundChild,
            child: Stack(
              children: [
                child,
                if (bottomNavBar != null) bottomNavBar!,
              ],
            ),
          ),
        ),
      ),
    );
  }
}

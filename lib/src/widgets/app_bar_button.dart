import 'package:flutter/material.dart';

class AppBarButton extends StatelessWidget {
  AppBarButton(
      this.icon, {
        super.key,
        this.size = const Size.square(50),
        this.onPressed,
        Color? primary,
        OutlinedBorder? shape,
        EdgeInsetsGeometry? padding,
      })  : primary = primary ?? Colors.white.withOpacity(.75),
        padding = padding ?? const EdgeInsets.symmetric(horizontal: 16.0),
        shape = shape ??
            const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(2.0)),
            );

  final void Function()? onPressed;
  final Widget icon;
  final Color primary;
  final Size? size;
  final OutlinedBorder shape;
  final EdgeInsetsGeometry padding;

  @override
  Widget build(BuildContext context) => TextButton(
    onPressed: onPressed,
    style: TextButton.styleFrom(
      foregroundColor: primary,
      minimumSize: size,
      padding: padding,
      shape: shape,
    ),
    child: icon,
  );
}
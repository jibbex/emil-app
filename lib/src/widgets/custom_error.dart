import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/widgets/emil_scaffold.dart';
import 'package:emil_app/src/widgets/glass_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomError extends StatelessWidget {
  final BuildContext context;
  final FlutterErrorDetails errorDetails;
  final Configuration _config;

  CustomError({
    super.key,
    required this.errorDetails,
    required this.context,
  }) : _config = context.watch<Configuration>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    EmilAppLocalizations localizations = EmilAppLocalizations.of(context)!;
    EdgeInsets spacing = getContainerSpacing(context);
    return EmilScaffold(
      child: GlassCard(
        margin: spacing,
        borderRadius: 16,
        backgroundColor: Colors.white.withOpacity(_config.opacity),
        child: CustomScrollView(slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              [
                getTitle(context, localizations.error),
                const Divider(),
                SingleChildScrollView(
                    child: Column(
                  children: [
                    getTitle(
                      context,
                      '(╯°□°）╯︵ ┻━┻',
                      const TextStyle(
                        fontSize: 32,
                      ),
                    ),
                    Text(
                      errorDetails.toString(),
                    ),
                  ],
                ))
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

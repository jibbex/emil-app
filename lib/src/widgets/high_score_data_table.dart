import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/high_scores/high_scores.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HighScoreDataTable extends StatelessWidget {
  const HighScoreDataTable({Key? key}) : super(key: key);

  TextStyle? getTextStyle(BuildContext context) =>
      Theme.of(context).textTheme.bodyLarge?.copyWith(
        fontWeight: FontWeight.w400,
        color: Colors.white70.withOpacity(0.6),
      );

  TextStyle? getTitleStyle(BuildContext context) =>
      getTextStyle(context)?.copyWith(
        fontSize: 16,
        fontWeight: FontWeight.w200,
      );

  @override
  Widget build(BuildContext context) {
    EmilAppLocalizations localizations = EmilAppLocalizations.of(context)!;
    HighScores data = context.watch<HighScores>();

    if (data.list.isEmpty) {
      data.loadData();
    }

    return SingleChildScrollView(
      child: data.list.isNotEmpty
          ? Material(
        type: MaterialType.transparency,
        color: Colors.transparent,
        surfaceTintColor: Colors.transparent,
        child: Flex(
          direction: Axis.horizontal,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: PaginatedDataTable(
                source: data,
                //headingTextStyle: Theme.of(context).textTheme.headline1,
                //dataTextStyle: getTextStyle(context),
                columns: <DataColumn>[
                  DataColumn(
                    label: Text(
                      localizations.name,
                      style: getTitleStyle(context),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      localizations.scores,
                      style: getTitleStyle(context),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      localizations.time,
                      style: getTitleStyle(context),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      localizations.tasks,
                      style: getTitleStyle(context),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )
          : const SizedBox(
        height: 340,
        width: 340,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
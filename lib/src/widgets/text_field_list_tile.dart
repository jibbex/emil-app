import 'package:emil_app/src/theme/colors.dart';
import 'package:flutter/material.dart';

class TextFieldListTile extends StatefulWidget {
  final Icon? leading;
  final String? hintText;
  final String? labelText;
  final String? initialValue;
  final int? maxLength;
  final Icon? trailing;
  final TextStyle? style;
  final EdgeInsets? contentPadding;
  final void Function(String)? onSubmitted;
  final void Function(String)? onChanged;
  final void Function()? onEditingComplete;
  final void Function(String?)? onSaved;
  final String? Function(String?)? validator;

  const TextFieldListTile({
    super.key,
    this.contentPadding,
    this.leading = const Icon(
      Icons.account_circle,
      size: 32,
    ),
    this.trailing = const Icon(
      Icons.edit_rounded,
      color: kPrimaryIconAlpha,
    ),
    this.hintText,
    this.maxLength,
    this.labelText,
    this.onSubmitted,
    this.onChanged,
    this.onSaved,
    this.style,
    this.onEditingComplete,
    this.initialValue,
    this.validator,
  });

  @override
  State<StatefulWidget> createState() => _TextFieldListTileState();
}

class _TextFieldListTileState extends State<TextFieldListTile> {
  late String text;
  var isEditable = false;

  void handleToggleClick() {
    if (isEditable && widget.onSaved != null) {
      widget.onSaved!(text);
    }
    setState(() => isEditable = !isEditable);
  }

  @override
  void initState() {
    text = widget.initialValue != null ? widget.initialValue! : '';
    super.initState();
  }


  Widget _constrained(Widget child) => ConstrainedBox(
        constraints: const BoxConstraints(
          minHeight: 50,
        ),
        child: child,
      );

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: widget.contentPadding,
      leading: _constrained(widget.leading as Widget),
      title: Container(
        margin: const EdgeInsets.only(right: 20),
        alignment: Alignment.center,
        height: 46,
        child: AnimatedCrossFade(
          duration: const Duration(milliseconds: 200),
          crossFadeState: isEditable ? CrossFadeState.showFirst : CrossFadeState.showSecond,
          firstChild: TextFormField(
            textAlignVertical: TextAlignVertical.center,
            initialValue: widget.initialValue,
            style: widget.style?.copyWith(
              fontSize: 18,
            ),
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.all(12),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(
                    color: kAltPrimary,
                  )),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(
                  color: kShadow.withOpacity(0.4),
                  width: 2,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(
                  color: kPrimary.withOpacity(0.8),
                  width: 4,
                ),
              ),
              hintText: widget.hintText,
              labelText: widget.labelText,
              hintStyle: widget.style?.copyWith(
                fontWeight: FontWeight.w600,
                color: kHint,
                shadows: [],
              ),
              isDense: true,
              filled: true,
              fillColor: kBackground.withOpacity(0.6),
            ),
            onFieldSubmitted: widget.onSubmitted,
            onChanged: (value) => setState(() {
              text = value;
            }),
            validator: widget.validator,
            maxLength: widget.maxLength,
          ),
          secondChild: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              text,
              style: widget.style?.copyWith(fontSize: 18),
            ),
          ),
          // transitionBuilder: (Widget child, Animation<double> animation) {
          //   return FadeTransition(opacity: animation, child: child);
          // },
        ),
      ),
      trailing: ElevatedButton(
        onPressed: handleToggleClick,
        child: !isEditable
            ? widget.trailing as Widget
            : const Icon(
                Icons.check_rounded,
                color: kPrimaryIconAlpha,
              ),
      ),
    );
  }
}

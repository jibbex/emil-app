import 'dart:io';

import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

/// Widget for selecting an image as avatar. Copies the image
/// to application support dir on Desktop and Mobile. On Web
/// a base64 encoded String of the data bytes in memory is
/// saved to local storage.
class AvatarSelect extends StatefulWidget {
  final ImageProvider? avatar;
  final void Function(String?)? onChanged;

  const AvatarSelect({
    super.key,
    this.avatar,
    this.onChanged,
  });

  @override
  State<AvatarSelect> createState() => _AvatarSelectState();
}

class _AvatarSelectState extends State<AvatarSelect> {
  Configuration? config;
  EmilAppLocalizations? local;
  ImageProvider? image;

  Future<void> handleOnTap() async {
    late final FilePickerResult? result;

    if (kIsWeb) {
      result = await FilePicker.platform.pickFiles();
    } else {
      result = await FilePicker.platform.pickFiles(
        dialogTitle: local?.selectAvatar,
        type: FileType.image,
        withData: true,
      );
    }

    if (widget.onChanged != null) {
      if (result != null) {
        final ext = result.files.first.extension;
        final filepath =
            kIsWeb ? result.files.first.bytes : result.files.first.path;

        if (kIsWeb) {
          final file = MemoryImage(filepath as Uint8List);
          config!.player.avatar.saveToLocalStorage(file, ext);
          widget.onChanged!('');
          setState(() {
            image = file;
          });
        } else if (filepath is String) {
          late File avatar;
          final path = await getApplicationSupportDirectory();
          var file = File(filepath);
          var avatarDir = Platform.isWindows ? path.absolute.uri.path.substring(1) : path.absolute.uri.path;

          if (Platform.isWindows || Platform.isLinux) {
            var oldFile = File('${avatarDir}avatar.$ext');

            if (await oldFile.exists()) {
              await oldFile.delete();
            }
          }

          avatar = await file.copy('${avatarDir}avatar.$ext');

          widget.onChanged!('');
          config!.player.avatar.filepath = avatar.path;
          setState(() {
            image = Image.file(file).image;
          });
        }
      }
    }
  }

  @override
  void initState() {
    image = widget.avatar;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    local ??= EmilAppLocalizations.of(context)!;
    config ??= context.watch<Configuration>();
    return Stack(
      children: [
        Center(
          child: Material(
            type: MaterialType.circle,
            clipBehavior: Clip.antiAlias,
            color: kOverlay.withOpacity(0.8),
            elevation: 8,
            child: AnimatedSwitcher(
              duration: const Duration(milliseconds: 600),
              transitionBuilder: (Widget child, Animation<double> animation) {
                return FadeTransition(opacity: animation, child: child);
              },
              switchInCurve: Curves.easeInCubic,
              switchOutCurve: Curves.easeOutCubic,
              child: image != null
                  ? Image(
                      key: ValueKey<ImageProvider>(image!),
                      image: image!,
                      width: 192,
                      height: 192,
                      fit: BoxFit.cover,
                    )
                  : SizedBox(
                      width: 192,
                      height: 192,
                      child: Icon(
                        Icons.person_off_rounded,
                        color: Colors.white.withOpacity(0.2),
                        size: 100,
                      ),
                    ),
            ),
          ),
        ),
        Align(
          alignment: const Alignment(0.9, 1),
          child: IconButton(
            onPressed: handleOnTap,
            style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                  backgroundColor: btnBackground,
                  fixedSize: const MaterialStatePropertyAll(Size(48, 48)),
                  padding: const MaterialStatePropertyAll(EdgeInsets.all(8)),
                  shadowColor: const MaterialStatePropertyAll(Colors.black),
                  shape: const MaterialStatePropertyAll(
                    CircleBorder(
                      side: BorderSide(
                        color: kOnPrimary,
                      ),
                    ),
                  ),
                  elevation: MaterialStateProperty.resolveWith(
                      (Set<MaterialState> states) {
                    if (states.contains(MaterialState.pressed)) {
                      return 2;
                    }
                    if (states.contains(MaterialState.hovered)) {
                      return 3;
                    }
                    return 1;
                  }),
                ),
            icon: const Icon(
              Icons.image_rounded,
              color: kPrimaryIconAlpha,
              size: 28,
            ),
          ),
        ),
      ],
    );
  }
}

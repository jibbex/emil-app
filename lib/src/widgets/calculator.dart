import 'dart:async';
import 'dart:math';
import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/models/tasks.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:emil_app/src/widgets/glass_card.dart';
import 'package:emil_app/src/widgets/num_pad.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class Calculator extends StatefulWidget {
  final void Function() onCorrect;
  final void Function(Tasks correctTaks) onEnd;
  final Configuration config;
  final Tasks tasks;
  final bool timerMode;

  const Calculator({
    super.key,
    this.timerMode = false,
    required this.config,
    required this.tasks,
    required this.onCorrect,
    required this.onEnd,
  });

  @override
  State<Calculator> createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> with TickerProviderStateMixin {
  int finishedTasks = 0;
  int correctResults = 0;
  String equation = '0 + 0';
  DateTime startTime = DateTime.now();
  late Tasks correctTasks;
  late Timer? interval;
  late Duration? duration;

  late final AnimationController controller = AnimationController(
    duration: const Duration(seconds: 1),
    vsync: this,
  );

  late final AnimationController offsetController = AnimationController(
    duration: const Duration(milliseconds: 750),
    vsync: this,
  );

  late final Animation<double> animation = CurvedAnimation(
    parent: controller,
    curve: Curves.fastOutSlowIn,
  );

  late final Animation<Offset> offsetAnimation = Tween(
    begin: const Offset(0.0, 2.5),
    end: Offset.zero,
  ).animate(CurvedAnimation(
    parent: offsetController,
    curve: Curves.fastOutSlowIn,
  ));

  Future<void> playCorrectAnimation() async {
    controller.reset();
    offsetController.reset();

    await Future.any([
      controller.forward(),
      offsetController.forward(),
    ]);

    controller.reverse();
  }

  Widget progressBuilder(BuildContext context) {
    late double progress;

    if (!widget.timerMode) {
      progress = finishedTasks / widget.tasks.length;
      return GlassCard(
        borderWidth: 0,
        backgroundColor: Colors.black38.withOpacity(0.05),
        margin: const EdgeInsets.only(top: 25),
        children: [
          Center(
            child: LinearProgressIndicator(
              minHeight: 10,
              value: progress,
              backgroundColor: Colors.transparent,
              color: kOnPrimary.withOpacity(0.2),
            ),
          ),
        ],
      );
    }

    final int total = widget.config.selectedDifficulty.timer!.value!.inSeconds;
    progress = duration!.inSeconds / total;

    interval ??= Timer.periodic(const Duration(seconds: 1), (timer) {
      if (duration!.inSeconds > 0) {
        setState(() {
          duration = Duration(seconds: duration!.inSeconds - 1);
        });
      } else {
        timer.cancel();
        //TODO: onEnd callback
      }
    });

    return Container(
      constraints: const BoxConstraints(
        maxWidth: 150,
        maxHeight: 112,
      ),
      child: GlassCard(
        borderRadius: 16,
        backgroundColor: Colors.white.withOpacity(widget.config.opacity),
        margin: const EdgeInsets.only(top: 15),
        children: [
          CircularPercentIndicator(
            animateFromLastPercent: true,
            animation: true,
            circularStrokeCap: CircularStrokeCap.round,
            backgroundColor: Colors.transparent,
            startAngle: 360,
            center: Icon(
              Icons.timer_rounded,
              size: 64,
              color: Colors.white.withOpacity(0.2),
            ),
            radius: 45,
            percent: progress,
            lineWidth: 8,
            progressColor: kPrimary.withOpacity(0.25),
          ),
        ],
      ),
    );
  }

  void handleChanged(String value, Function? callback) {
    final difficulty = widget.config.selectedDifficulty;
    widget.tasks.current.value = int.parse(value);
    setState(() => ++finishedTasks);

    if (widget.tasks.isCorrect()) {
      playCorrectAnimation();
      correctTasks.add(widget.tasks.current);
      setState(() => ++correctResults);

      if (widget.timerMode) {
        setState(() {
          final maxSec = difficulty.timer!.value!.inSeconds;
          final bonus = difficulty.timer!.bonus!.inSeconds;
          final seconds = duration!.inSeconds + bonus;
          duration = Duration(seconds: seconds > maxSec ? maxSec : seconds);
        });
      }

      widget.onCorrect();
    }

    if (widget.tasks.moveNext()) {
      setState(() => equation = widget.tasks.current.toString());
    } else if (widget.timerMode) {
      final index = Random().nextInt(difficulty.getPatterns.length);
      widget.tasks.generate(20, difficulty[index]);
      widget.tasks.moveNext();
      setState(() => equation = widget.tasks.current.toString());
    } else {
      // TODO: onEnd callback
    }

    if (callback != null) {
      callback();
    }
  }

  @override
  void initState() {
    correctTasks = Tasks();
    if (widget.timerMode) {
      duration = widget.config.selectedDifficulty.timer!.value;
    }
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    offsetController.dispose();
    interval?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO:animation on correct callback in parent.
    return Column(
      children: [
        NumPad(
          onChanged: handleChanged,
          equation: equation,
        ),
        progressBuilder(context),
      ],
    );
  }
}
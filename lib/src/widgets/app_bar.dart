import 'package:flutter/material.dart';

/// Wrapper arrow function for the SliverAppBar widget,
/// that will be passed to each view widget.
SliverAppBar appBar({
  required BuildContext context,
  required String title,
  Widget? leading,
  List<Widget>? actions,
}) =>
    SliverAppBar(
      title: Text(
        title,
        style: TextStyle(
          color: Colors.white.withOpacity(.5),
          fontSize: 22,
          fontStyle: FontStyle.italic,
        ),
      ),
      leading: (leading != null && Navigator.of(context).canPop())
          ? Padding(
        padding: const EdgeInsets.all(5),
        child: TextButton(
          onPressed: () => Navigator.of(context).pop(),
          style: TextButton.styleFrom(
            fixedSize: const Size.square(50),
            foregroundColor: Colors.white.withOpacity(.5),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
          ),
          child: const Icon(Icons.arrow_back_ios_new_rounded),
        ),
      )
          : null,
      automaticallyImplyLeading: false,
      actions: actions,
      expandedHeight: 60,
      surfaceTintColor: Colors.black.withOpacity(0),
      backgroundColor: Colors.black.withOpacity(0.0),
      foregroundColor: Colors.white.withOpacity(0.5),
      // shadowColor: Colors.white.withOpacity(0.5),
      // forceMaterialTransparency: true,
      // floating: true,
      // stretchTriggerOffset: 60,
      // onStretchTrigger: () {
      //   print("Stretch Trigger called");
      //   return Future.value(null);
      // },
    );
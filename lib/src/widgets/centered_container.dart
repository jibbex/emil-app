import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/theme.dart';
import 'package:emil_app/src/widgets/glass_card.dart';
import 'package:flutter/material.dart';

class CenteredContainer extends StatelessWidget {
  final Widget child;
  final Configuration config;
  final double? height;

  const CenteredContainer({
    super.key,
    this.height,
    required this.child,
    required this.config,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final spacing = getContainerSpacing(context);

    return SizedBox(
      height: size.height - 100,
      width: size.width,
      child: Column(
        children: [
          const Spacer(),
          GlassCard(
            margin: spacing,
            borderRadius: 16,
            backgroundColor: Colors.white.withOpacity(config.opacity),
            children: [
              Padding(
                padding: const EdgeInsets.all(25),
                child: SizedBox(
                  height: height ?? size.height * 0.5,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(16)),
                    child: child,
                  ),
                ),
              ),
            ],
          ),
          const Spacer(),
        ],
      ),
    );
  }
}

import 'package:emil_app/src/models/configuration.dart';
import 'package:emil_app/src/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:glass_kit/glass_kit.dart';
import 'package:provider/provider.dart';

class GlassButton extends StatelessWidget {
  final bool glass;
  final double width;
  final double height;
  final Widget child;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final double? borderRadius;
  final void Function()? onPressed;
  final Map<String, double>? opacities;

  const GlassButton({
    super.key,
    required this.child,
    required this.onPressed,
    required this.width,
    required this.height,
    this.glass = true,
    this.opacities,
    this.margin,
    this.padding,
    this.borderRadius,
  });

  Future<void> _onTapDown(_) async {
    if (kIsWeb && onPressed != null) {
      await Future.delayed(const Duration(milliseconds: 100));
      onPressed!();
    }
  }

  Future<void> _onTapUp(_) async {
    if (!kIsWeb && onPressed != null) {
      await Future.delayed(const Duration(milliseconds: 100));
      onPressed!();
    }
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = context.read<Configuration>();
    Map<String, double> op = opacities != null
        ? opacities!
        : <String, double>{
            'gradient1': config.opacity + 0.05,
            'gradient2': config.opacity,
            'borderGradient1': config.opacity + 0.15,
            'borderGradient2': config.opacity,
            'borderGradient3': config.opacity + 0.05,
            'borderGradient4': config.opacity + 0.15,
            'shadow': config.opacity + 0.15,
            'border': config.opacity + 0.1,
            'frosted': config.opacity < 0.15 ? config.opacity : 0.15,
          };

    return GlassContainer(
      height: height,
      width: width,
      gradient: LinearGradient(
        colors: [
          Colors.white.withOpacity(op['gradient1']!),
          Colors.white.withOpacity(op['gradient2']!)
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      ),
      borderGradient: !kIsWeb
          ? LinearGradient(
              colors: [
                Colors.deepOrange.shade200.withOpacity(op['borderGradient1']!),
                Colors.deepOrange.shade200.withOpacity(op['borderGradient2']!),
                Colors.deepOrange.shade200.withOpacity(op['borderGradient3']!),
                Colors.deepOrange.shade300.withOpacity(op['borderGradient4']!)
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: const [0.0, 0.39, 0.40, 1.0],
            )
          : null,
      borderColor:
          kIsWeb ? Colors.deepOrange.shade200.withOpacity(op['border']!) : null,
      blur: 12.0,
      borderWidth: 4.0,
      elevation: 18.0,
      isFrostedGlass: glass,
      shadowColor: Colors.black.withOpacity(op['shadow']!),
      borderRadius: BorderRadius.all(
        Radius.circular(borderRadius ?? 20),
      ),
      alignment: Alignment.center,
      frostedOpacity: op['frosted']!,
      margin: margin ?? const EdgeInsets.all(2.5),
      padding: padding ?? const EdgeInsets.all(0),
      child: Material(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 20),
        ),
        color: Colors.white.withOpacity(0.0),
        clipBehavior: Clip.hardEdge,
        child: InkWell(
          splashColor: kPrimary.withOpacity(0.2),
          hoverColor: kLightBackground.withOpacity(0.05),
          highlightColor: kPrimary.withOpacity(0.05),
          onTapDown: _onTapDown,
          onTapUp: _onTapUp,
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class Qr extends StatelessWidget {
  final String data;
  final bool active;
  final double? size;
  final EdgeInsets padding;
  final BorderRadius borderRadius;
  final Color foregroundColor;
  final Color backgroundColor = const Color(0x21ffffff);
  final Widget child;

  const Qr({
    super.key,
    this.size,
    this.active = false,
    required this.data,
    required this.child,
    required this.padding,
    required this.borderRadius,
    required this.foregroundColor,
  });

  @override
  Widget build(BuildContext context) {
    if (active) {
      return child;
    }

    return ClipRRect(
      borderRadius: borderRadius,
      child: QrImageView(
        padding: padding,
        size: size,
        errorCorrectionLevel: QrErrorCorrectLevel.Q,
        gapless: false,
        data: data,
        eyeStyle: QrEyeStyle(
          eyeShape: QrEyeShape.circle,
          color: foregroundColor,
        ),
        dataModuleStyle: QrDataModuleStyle(
          color: foregroundColor,
        ),
        backgroundColor: backgroundColor,
      ),
    );
  }
}

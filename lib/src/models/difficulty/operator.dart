import 'package:emil_app/src/l10n/emil_app_localizations.dart';

enum Operators {
  add,
  subtract,
  multiply,
  divide,
}

class Operator {
  final String? name;
  final String symbol;
  final String exprSymbol;
  final Operators operator;

  Operator({
    this.name,
    required this.symbol,
    required this.operator,
    required this.exprSymbol,
  });

  factory Operator.fromOperators(Operators operator,
      [EmilAppLocalizations? localizations]) {
    String? name;
    String symbol;
    String exprSymbol;

    switch (operator) {
      case Operators.add:
        name = localizations?.addition;
        exprSymbol = symbol = "+";
        break;
      case Operators.subtract:
        name = localizations?.subtract;
        exprSymbol = symbol = "-";
        break;
      case Operators.multiply:
        name = localizations?.multiply;
        symbol = "•";
        exprSymbol = "*";
        break;
      case Operators.divide:
        name = localizations?.divide;
        symbol = ":";
        exprSymbol = "/";
        break;
      default:
        throw ArgumentError("Operator not defined.", "operator");
    }

    return Operator(
      name: name,
      symbol: symbol,
      exprSymbol: exprSymbol,
      operator: operator,
    );
  }

  factory Operator.fromJSON(Map map) => Operator(
        name: map['name'],
        symbol: map['symbol'],
        exprSymbol: map['exprSymbol'],
        operator: Operators.values[map['operator']],
      );

  Map toJSON() => {
        'name': name,
        'symbol': symbol,
        'exprSymbol': exprSymbol,
        'operator': operator.index,
      };
}

import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/difficulty/difficulty_pattern.dart';
import 'package:emil_app/src/models/difficulty/emil_timer.dart';
import 'package:flutter/material.dart';

class Difficulty extends ChangeNotifier  {
  String title;
  int taskCount;
  EmilTimer? _timer;
  final List<DifficultyPattern> _patterns;

  Difficulty({
    required this.title,
    required this.taskCount,
    required EmilAppLocalizations localizations,
    EmilTimer? timer,
    List<DifficultyPattern>? patterns,
  })  : _timer = timer,
        _patterns = patterns ??
            <DifficultyPattern>[
              DifficultyPattern(minimum: 1, maximum: 50, maxOperands: 2, localizations: localizations)
            ];

  List<DifficultyPattern> get getPatterns => _patterns;
  EmilTimer? get timer => _timer;
  DifficultyPattern operator[](int index) => _patterns[index];

  void addPattern(DifficultyPattern pattern) {
    _patterns.add(pattern);
    notifyListeners();
  }

  DifficultyPattern removePatternAt(int index) {
    DifficultyPattern pattern = _patterns.removeAt(index);
    notifyListeners();
    return pattern;
  }

  void setTimer({Duration? value, Duration? bonus}) {
    if (_timer == null) {
      _timer ??= EmilTimer(value: value, bonus: bonus);
      notifyListeners();
      return;
    }

    if (value != null) {
      _timer?.value ??= value;
    }

    if (bonus != null) {
      _timer?.bonus ??= bonus;
    }

    notifyListeners();
  }

  Map<String, dynamic> toMap() => {
    'title': title,
    'taskCount': taskCount,
    'timer': <String, dynamic> {
      'value': _timer?.value,
      'bonus': _timer?.bonus,
    },
  };
}

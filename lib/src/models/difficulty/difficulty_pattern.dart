import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/difficulty/operator.dart';
import 'package:flutter/material.dart';

class DifficultyPattern extends ChangeNotifier {
  int _minimum;
  int _maximum;
  int _maxOperands;
  bool _hasNegativeResult;
  final List<Operator> _operators;

  DifficultyPattern({
    required int minimum,
    required int maximum,
    required int maxOperands,
    required EmilAppLocalizations localizations,
    bool? hasNegativeResult,
    List<Operator>? operators,
  })  : _minimum = minimum,
        _maximum = maximum,
        _maxOperands = maxOperands,
        _hasNegativeResult = hasNegativeResult ?? false,
        _operators = operators ??
            List.generate(
              2,
              (index) => Operator.fromOperators(
                  Operators.values[index], localizations),
            );

  factory DifficultyPattern.fromMap(
    EmilAppLocalizations locale,
    Map<String, dynamic> map,
  ) =>
      DifficultyPattern(
        minimum: map['minimum'],
        maximum: map['maximum'],
        maxOperands: map['maxOperands'],
        operators: (map['operators'] as List<int>)
            .map(
              (index) =>
                  Operator.fromOperators(Operators.values[index], locale),
            )
            .toList(),
        localizations: locale,
      );

  set minimum(int minimum) {
    _minimum = minimum;
    notifyListeners();
  }

  set maximum(int maximum) {
    _maximum = maximum;
    notifyListeners();
  }

  set maxOperands(int maxOperands) {
    _maxOperands = maxOperands;
    notifyListeners();
  }

  set hasNegativeResult(bool hasNegativeResults) {
    _hasNegativeResult = hasNegativeResults;
    notifyListeners();
  }

  void addOperator(Operator operator) => _operators.add(operator);

  Operator removeOperatorAt(int index) => _operators.removeAt(index);

  Operator operator [](int index) => _operators[index];

  List<Operator> get operators => _operators;

  int get minimum => _minimum;

  int get maximum => _maximum;

  int get maxOperands => _maxOperands;

  bool get hasNegativeResult => _hasNegativeResult;

  Map<String, dynamic> toMap() => {
        'minimum': minimum,
        'maximum': maximum,
        'maxOperands': maxOperands,
        'hasNegativeResult': hasNegativeResult,
        'operators': _operators.map((operator) => operator.operator.index),
      };
}

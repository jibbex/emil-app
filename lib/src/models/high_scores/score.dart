import 'dart:math';
import 'package:emil_app/src/extensions/duration_apis.dart';
import 'package:emil_app/src/models/difficulty/operator.dart';
import 'package:emil_app/src/models/high_scores/num_points.dart';
import 'package:emil_app/src/models/task.dart';
import 'package:emil_app/src/models/tasks.dart';
import 'package:flutter/material.dart';

@immutable
class Score {
  static const int _kAddPoints = 1;
  static const int _kSubtractPoints = 1;
  static const int _kMultiplyPoints = 2;
  static const int _kDivisionPoints = 2;
  static final List<NumPoints> _kNumPoints = <NumPoints>[
    NumPoints(0, 1),
    NumPoints(25, 2),
    NumPoints(75, 3),
    NumPoints(150, 4),
  ];

  final int score;
  final Duration duration;

  factory Score(Tasks tasks, Duration duration) {
    int score = 0;

    for (Task task in tasks) {
      score += _calculate(task);
    }

    return Score._(
      score,
      Duration(
        seconds: duration.inSeconds.abs(),
      ),
    );
  }

  factory Score.fromJson(Map<String, dynamic> map) => Score._(
        map['score'],
        Duration(seconds: map['duration'] ?? 0),
      );

  const Score._(this.score, this.duration);

  String get formattedTime => duration.formattedString;

  Map<String, dynamic> toJson() => <String, dynamic>{
        'score': score,
        'duration': duration.inSeconds,
      };

  static int _calculate(Task task) {
    int scores = 0;

    for (final op in task.toString().split(' ')) {
      int numPoints = 0;
      final number = num.tryParse(op);

      if (number != null) {
        for (NumPoints numPoint in _kNumPoints) {
          if (numPoint < number) {
            numPoints = numPoint.points;
          }
        }
      } else {
        switch (op) {
          case '+':
            numPoints += _kAddPoints;
            break;
          case '-':
            numPoints += _kSubtractPoints;
            break;
          case '•':
            numPoints += _kMultiplyPoints;
            break;
          case ':':
            numPoints += _kDivisionPoints;
        }
      }

      scores += numPoints;
    }

    return scores;
  }
}

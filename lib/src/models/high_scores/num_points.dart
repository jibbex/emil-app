class NumPoints {
  late num number;
  late int points;

  NumPoints(this.number, this.points);

  @override
  bool operator ==(Object other) => number == other;

  bool operator >=(num other) => other <= number;
  bool operator >(num other) => other < number;
  bool operator <=(num other) => other >= number;
  bool operator <(num other) => other > number;

  @override
  int get hashCode => number.hashCode;

}
import 'dart:convert';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:emil_app/src/models/pages.dart';
import 'package:emil_app/src/models/player.dart';
import 'package:emil_app/src/extensions/localstorage_apis.dart';

class HighScores extends DataTableSource {
  static const String kStore = 'high_scores';
  String _store;
  List<Player> _scores;
  Pages<Player> _pages;

  HighScores()
      : _store = kStore,
        _scores = [],
        _pages = Pages<Player>();

  HighScores.fromIterable(Iterable scores)
      : _store = kStore,
        _scores = scores.toList() as List<Player>,
        _pages = Pages.fromList(scores.toList() as List<Player>);


  Player operator [](int index) => _scores[index];

  static Future<HighScores> load([String? store]) async {
    LocalStorage storage = await StorageFactory.init(store ?? kStore, 'emil-app');
    var data = storage.getItem('scores');
    List<Map<String, dynamic>> jsonArr = jsonDecode(data);
    return HighScores.fromIterable(jsonArr);
  }

  Future<void> loadData([String? store]) async {
    if (store != null) {
      _store = store;
    }

    LocalStorage storage = await StorageFactory.init(store ?? kStore, 'emil-app');
    String? data = storage.getItem('scores');

    List<dynamic> json = jsonDecode(data ?? '[]');
    _scores = json.map((e) => Player.fromMap(e as Map<String, dynamic>)).toList();
    _pages = Pages.fromList(_scores);
    notifyListeners();
  }

  Future<void> save() async {
    try {
      LocalStorage storage = await StorageFactory.init(_store, 'emil-app');
      String data = jsonEncode(toJson());
      await storage.setItem('scores', data);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }

  int get scoreIndex => _store == 'high_scores' ? 0 : 1;
  int get playerPerPage => _pages.itemsPerPage;
  Pages get pages => _pages;
  List<Map<String, dynamic>> toJson() =>
      _scores.map((e) => e.toMap()).toList();
  List<Player> get list => _scores;

  void add(Player player) {
    _scores.add(player);
    save();
    notifyListeners();
  }

  @override
  DataRow? getRow(int index) {
    assert(index >= 0);

    if (index >= _scores.length) {
      return null;
    }

    final player = _scores[index];

    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text(player.name)),
        DataCell(Text(player.score!.score.toString())),
        DataCell(Text(player.score!.formattedTime)),
        DataCell(Text(player.taskCount.toString())),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _scores.length;

  @override
  int get selectedRowCount => 0;

  void sort<T>(Comparable<T> Function(Player player) getField, bool asc) {
    _scores.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);

      return asc
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });

    notifyListeners();
  }
}

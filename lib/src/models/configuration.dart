import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:emil_app/src/models/player.dart';
import 'package:emil_app/src/l10n/emil_app_localizations.dart';
import 'package:emil_app/src/models/difficulty/difficulty.dart';
import 'package:emil_app/src/models/difficulty/difficulty_pattern.dart';
import 'package:emil_app/src/models/difficulty/operator.dart';
import 'package:emil_app/src/models/difficulty/emil_timer.dart';
import 'package:emil_app/src/extensions/localstorage_apis.dart';

class Configuration extends ChangeNotifier {
  late EmilAppLocalizations? localizations;
  late _InitConfiguration? _state;

  Configuration(this.localizations) {
    if (localizations != null) {
      _state = _InitConfiguration(
        localizations!,
        player: BasePlayer(name: 'Player', notifier: notifyListeners),
      );
    }
  }

  Configuration.fromJson(Map<String, dynamic> json, this.localizations) {
    if (localizations != null) {
      _state = _InitConfiguration(
        localizations!,
        player: BasePlayer(name: 'Player', notifier: notifyListeners),
      );
    }

    if (isInitialized) {
      _fromJson(json);
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'isTimerActive': isTimerActive,
        'isCorrectMistakesActive': isCorrectMistakesActive,
        'hasAnimatedBackground': hasAnimatedBackground,
        'isGlassMorphismActivated': isGlassMorphismActivated,
        'isShaderActive': isShaderActive,
        'opacity': opacity,
        'selectedDifficulty': _state!._selectedDifficultyIndex,
        'player': _state!.player.toMap(),
      };

  void initState(EmilAppLocalizations localizations) {
    _state = _InitConfiguration(
      localizations,
      player: BasePlayer(name: 'Player', notifier: notifyListeners),
    );
  }

  Future<void> load() async {
    LocalStorage storage = await StorageFactory.init('config', 'emil-app');
    _fromJson(storage.getItem('config'));
    notifyListeners();
  }

  Future<void> save() async {
    LocalStorage storage = await StorageFactory.init('config', 'emil-app');
    await storage.setItem('config', toJson());
  }

  void _fromJson(Map<String, dynamic> json) {
    setTimerActive = json['isTimerActive'];
    setAnimatedBackground = json['hasAnimatedBackground'];
    setCorrectMistakesActive = json['isCorrectMistakesActive'];
    setGlassMorphismActivate = json['isGlassMorphismActivated'];
    setShaderActive = json['isShaderActive'];
    player = BasePlayer.fromMap(json['player']);
    opacity = json['opacity'];
    _state!._selectedDifficultyIndex = json['selectedDifficulty'];
  }

  bool get isInitialized => _state != null;

  bool get hasAnimatedBackground => _state!.hasAnimatedBackground;

  bool get isGlassMorphismActivated => _state!.isGlassMorphismActivated;

  bool get isTimerActive => _state!.isTimerActive;

  bool get isShaderActive => _state!.isShaderActive;

  bool get isCorrectMistakesActive => _state!.isCorrectMistakesActive;

  double get opacity => _state!.opacity;

  BasePlayer get player =>
      _state?.player ??
      BasePlayer(
        name: 'Player',
        notifier: notifyListeners,
      );

  Difficulty get selectedDifficulty => _state!.selectedDifficulty;

  List<Difficulty> get difficulties => _state!.difficulties;

  Difficulty getDifficultyAt(int index) => _state!.difficulties[index];

  Difficulty removeDifficultyAt(int index) {
    var difficulty = _state!.difficulties.removeAt(index);
    notifyListeners();
    return difficulty;
  }

  void addDifficulty(Difficulty difficulty) {
    _state!.difficulties.add(difficulty);
    notifyListeners();
  }

  set opacity(double opacity) {
    assert(opacity >= 0.05 && opacity <= 0.25,
        'The provided value is out of range');
    _state!.opacity = opacity;
    notifyListeners();
  }

  set setAnimatedBackground(bool hasAnimatedBackground) {
    _state!.hasAnimatedBackground = hasAnimatedBackground;
    notifyListeners();
  }

  set setGlassMorphismActivate(bool isGlassMorphismActivated) {
    _state!.isGlassMorphismActivated = isGlassMorphismActivated;
    notifyListeners();
  }

  set setShaderActive(bool isShaderActive) {
    _state!.isShaderActive = isShaderActive;
    notifyListeners();
  }

  set setTimerActive(bool isTimerActive) {
    _state!.isTimerActive = isTimerActive;
    notifyListeners();
  }

  set setCorrectMistakesActive(bool isCorrectMistakesActive) {
    _state!.isCorrectMistakesActive = isCorrectMistakesActive;
    notifyListeners();
  }

  set selectedDifficulty(Difficulty difficulty) {
    _state!.selectDifficulty = difficulty;
    notifyListeners();
  }

  set player(BasePlayer player) {
    _state!.player = player;
    notifyListeners();
  }

  int getIndexOf(Difficulty difficulty) =>
      _state!.difficulties.indexOf(difficulty);
}

class _InitConfiguration {
  bool hasAnimatedBackground = true;
  bool isGlassMorphismActivated = true;
  bool isCorrectMistakesActive = false;
  bool isTimerActive = false;
  bool isShaderActive = true;
  int _selectedDifficultyIndex = 0;
  double opacity = 0.05;
  late BasePlayer player;
  final EmilAppLocalizations localizations;
  late final List<Difficulty> difficulties;

  _InitConfiguration(this.localizations, {required this.player}) {
    difficulties = <Difficulty>[
      Difficulty(
        title: localizations.difficultyEasy,
        taskCount: 20,
        timer: EmilTimer(
          value: const Duration(minutes: 1),
          bonus: const Duration(seconds: 3),
        ),
        localizations: localizations,
        patterns: <DifficultyPattern>[
          DifficultyPattern(
              minimum: 1,
              maximum: 50,
              maxOperands: 2,
              hasNegativeResult: false,
              localizations: localizations,
              operators: [
                Operator.fromOperators(Operators.add, localizations),
                Operator.fromOperators(Operators.subtract, localizations),
              ]),
        ],
      ),
      Difficulty(
        title: localizations.difficultyMedium,
        taskCount: 75,
        timer: EmilTimer(
          value: const Duration(seconds: 45),
          bonus: const Duration(seconds: 3),
        ),
        localizations: localizations,
        patterns: <DifficultyPattern>[
          DifficultyPattern(
            minimum: 1,
            maximum: 100,
            maxOperands: 3,
            hasNegativeResult: false,
            localizations: localizations,
            operators: [
              Operator.fromOperators(Operators.add, localizations),
              Operator.fromOperators(Operators.subtract, localizations),
            ],
          ),
          DifficultyPattern(
            minimum: 1,
            maximum: 10,
            maxOperands: 2,
            localizations: localizations,
            operators: [
              Operator.fromOperators(Operators.multiply, localizations),
              Operator.fromOperators(Operators.divide, localizations),
            ],
          ),
        ],
      ),
      Difficulty(
        title: localizations.difficultyHard,
        taskCount: 150,
        timer: EmilTimer(
          value: const Duration(seconds: 30),
          bonus: const Duration(seconds: 2),
        ),
        localizations: localizations,
        patterns: <DifficultyPattern>[
          DifficultyPattern(
            minimum: 1,
            maximum: 200,
            maxOperands: 4,
            hasNegativeResult: true,
            localizations: localizations,
            operators: [
              Operator.fromOperators(Operators.add, localizations),
              Operator.fromOperators(Operators.subtract, localizations),
            ],
          ),
          DifficultyPattern(
            minimum: 1,
            maximum: 10,
            maxOperands: 2,
            hasNegativeResult: false,
            localizations: localizations,
            operators: [
              Operator.fromOperators(Operators.multiply, localizations),
              Operator.fromOperators(Operators.divide, localizations),
            ],
          ),
        ],
      ),
    ];
  }

  Difficulty get selectedDifficulty => difficulties[_selectedDifficultyIndex];

  set selectDifficulty(Difficulty difficulty) {
    _selectedDifficultyIndex = difficulties.indexOf(difficulty);
  }
}

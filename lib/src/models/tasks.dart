import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:emil_app/src/models/difficulty/difficulty_pattern.dart';
import 'package:emil_app/src/models/task.dart';

class Tasks with ChangeNotifier implements Iterable<Task> {
  int _cursor = -1;
  late List<Task> _tasks;

  Tasks({int? count, DifficultyPattern? pattern, List<Task>? tasks}) {
    if (count != null && pattern != null) {
      _tasks = List<Task>.generate(count, (_) => Task(pattern));
      notifyListeners();
    } else if (tasks != null) {
      _tasks = tasks;
      notifyListeners();
    } else {
      _tasks = [];
    }
  }

  factory Tasks.fromList(List<dynamic> tasks) => Tasks(
      tasks: tasks
          .map<Task>((e) => Task()..expression = jsonDecode(e)!['expression'])
          .toList());

  Task get current => _tasks.elementAt(_cursor);

  bool isCorrect() => current.value == current.result;

  bool moveNext() {
    if (_cursor < _tasks.length - 1) {
      _cursor++;
      notifyListeners();
      return true;
    }

    return false;
  }

  Tasks get correctTasks {
    final tasks = Tasks();

    _tasks.where((e) => e.value == e.result).forEach((e) => tasks.add(e));

    return tasks;
  }

  void add(Task task) => _tasks.add(task);

  void generate(int count, DifficultyPattern pattern) {
    _tasks.addAll(List<Task>.generate(count, (_) => Task(pattern)));
    notifyListeners();
  }

  Task operator [](int index) => _tasks[index];

  @override
  int get length => _tasks.length;

  @override
  bool any(bool Function(Task element) test) => _tasks.any(test);

  @override
  Iterable<R> cast<R>() => _tasks.cast();

  @override
  bool contains(Object? element) => _tasks.contains(element);

  @override
  Task elementAt(int index) => _tasks[index];

  @override
  bool every(bool Function(Task element) test) => _tasks.every(test);

  @override
  Iterable<T> expand<T>(Iterable<T> Function(Task element) toElements) =>
      _tasks.expand(toElements);

  @override
  Task get first => _tasks.first;

  @override
  Task firstWhere(bool Function(Task element) test,
          {Task Function()? orElse}) =>
      _tasks.firstWhere(test, orElse: orElse);

  @override
  T fold<T>(
          T initialValue, T Function(T previousValue, Task element) combine) =>
      _tasks.fold(initialValue, combine);

  @override
  Iterable<Task> followedBy(Iterable<Task> other) => _tasks.followedBy(other);

  @override
  void forEach(void Function(Task element) action) => _tasks.forEach(action);

  @override
  bool get isEmpty => _tasks.isEmpty;

  @override
  bool get isNotEmpty => _tasks.isNotEmpty;

  @override
  Iterator<Task> get iterator => _tasks.iterator;

  @override
  String join([String separator = ""]) => _tasks.join(separator);

  @override
  Task get last => _tasks.last;

  @override
  Task lastWhere(bool Function(Task element) test, {Task Function()? orElse}) =>
      _tasks.lastWhere(test);

  @override
  Iterable<T> map<T>(T Function(Task e) toElement) => _tasks.map(toElement);

  @override
  Task reduce(Task Function(Task value, Task element) combine) =>
      _tasks.reduce(combine);

  @override
  Task get single => _tasks.single;

  @override
  Task singleWhere(bool Function(Task element) test,
          {Task Function()? orElse}) =>
      _tasks.singleWhere(test, orElse: orElse);

  @override
  Iterable<Task> skip(int count) => _tasks.skip(count);

  @override
  Iterable<Task> skipWhile(bool Function(Task value) test) =>
      _tasks.skipWhile(test);

  @override
  Iterable<Task> take(int count) => _tasks.skip(count);

  @override
  Iterable<Task> takeWhile(bool Function(Task value) test) =>
      _tasks.takeWhile(test);

  @override
  List<Task> toList({bool growable = true}) =>
      _tasks.toList(growable: growable);

  @override
  Set<Task> toSet() => _tasks.toSet();

  @override
  Iterable<Task> where(bool Function(Task element) test) => _tasks.where(test);

  @override
  Iterable<T> whereType<T>() => _tasks.whereType<T>();

  @override
  String toString() {
    String json = '[';

    for (final task in _tasks) {
      json += '${task.toJson()}, ';
    }

    json = '${json.substring(json.length - 2)}]';

    return json;
  }
}

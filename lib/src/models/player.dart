import 'dart:convert';
import 'dart:io';

import 'package:emil_app/src/extensions/localstorage_apis.dart';
import 'package:emil_app/src/models/high_scores/score.dart';
import 'package:emil_app/src/models/tasks.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:uuid/uuid.dart';

/// In order to update game independent data without initializing game
/// related data, BasePlayer class has the properties id, name and avatar.
/// The id is a String of a unique identifier, name is a String and avatar
/// is a nullable instance of Avatar.
class BasePlayer {
  /// A String of a unique identifier
  late String id;

  /// A String of users name. It will be displayed in opponents high
  /// scores tables.
  String _name;

  /// A nullable instance of Avatar with an property of image data
  Avatar avatar;

  /// ChangeNotifier callback that is invoked on changes
  void Function()? notifier;

  /// Creates an BasePlayer object from the passed arguments. The file
  /// argument is the filepath of the avatar image. It is used to
  /// initialize an Avatar object.
  BasePlayer({
    required String name,
    this.notifier,
    String? id,
    String? file,
    Avatar? image,
  })  : id = id != null && id.isEmpty ? id : const Uuid().v4(),
        _name = name,
        avatar = image ?? Avatar(file);

  String get name => _name;

  set name(String username) {
    _name = username;
    if (notifier != null) {
      notifier!();
    }
  }

  factory BasePlayer.fromMap(Map<String, dynamic> map) => BasePlayer(
        id: map['id'],
        name: map['name'],
        file: map['avatar'],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'avatar': avatar.filepath,
      };
}

/// Player extends the BasePlayer class with game related data
class Player extends BasePlayer {
  Score? score;
  bool timingMode;
  int taskCount;
  int correctCount;
  int difficulty;
  DateTime? startTime;
  Tasks tasks;
  Tasks finishedTasks;

  Player({
    required super.id,
    required super.name,
    super.file,
    super.image,
    this.score,
    this.startTime,
    this.difficulty = 0,
    this.taskCount = 0,
    this.correctCount = 0,
    this.timingMode = false,
    Tasks? finished,
    Tasks? tasks,
  })  : finishedTasks = finished ?? Tasks(),
        tasks = tasks ?? Tasks();

  factory Player.fromBasePlayer(BasePlayer player) => Player(
        id: const Uuid().v4(),
        name: player.name,
        file: player.avatar.filepath,
      );

  factory Player.fromMap(Map<String, dynamic> map) => Player(
        id: map['id'],
        name: map['name'],
        image: map['avatarImage'],
        score: map['score'] != null ? Score.fromJson(map['score']) : null,
        taskCount: map['taskCount'] ?? 0,
        correctCount: map['correctCount'] ?? 0,
        startTime: DateTime.fromMicrosecondsSinceEpoch(map['startTime'] ?? 0),
        difficulty: map['difficulty'] ?? 0,
        timingMode: map['timingMode'] ?? false,
        finished:
            map['finished'] != null ? Tasks.fromList(map['finished']) : Tasks(),
        tasks: map['tasks'] != null ? Tasks.fromList(map['tasks']) : Tasks(),
      );

  @override
  Map<String, dynamic> toMap() => super.toMap()
    ..addAll({
      'score': score?.toJson(),
      'taskCount': taskCount,
      'correctCount': correctCount,
      'startTime': startTime?.microsecondsSinceEpoch,
      'difficulty': difficulty,
      'timingMode': timingMode,
      'finished': finishedTasks.map((e) => e.toJson()).toList(),
      'tasks': tasks.map((e) => e.toJson()).toList(),
    });
}

/// Stores image filepath and loads the image from the storage.
class Avatar {
  void Function()? notifier;
  ImageProvider? _image;
  String? _path;

  Avatar([String? filepath, this.notifier]) {
    if (filepath != null) {
      this.filepath = filepath;
    }
  }

  factory Avatar.fromBase64(String image) => Avatar().._load(image);

  set filepath(String? filepath) {
    _path = filepath;
    _load();
  }

  String? get filepath => _path;

  ImageProvider? get image => _image;

  /// Loads avatar file from [_path] or [LocalStorage] encodes it to
  /// base64 and returns the encoded [String]. If file doesn't exist,
  /// an [NotFoundException] exception is thrown.
  Future<String?> toBase64() async {
    if (_path != null) {
      String img = 'data:image/png;base64,';

      if (!kIsWeb) {
        final file = File(_path!);

        if (await file.exists()) {
          final bytes = await file.readAsBytes();
          img += base64Encode(bytes);
        } else {
          throw NotFoundException(_path!);
        }
      } else {
        LocalStorage storage = await StorageFactory.init('player', 'emil-app');
        final file = storage.getItem('avatar');

        if (file != null) {
          img += file;
        } else {
          return null;
        }
      }

      return img;
    }

    return null;
  }

  void saveToLocalStorage(MemoryImage file, [String? type]) async {
    if (kIsWeb) {
      var data = 'data:image/${type ?? 'png'};base64,${base64Encode(file.bytes)}';
      LocalStorage storage = await StorageFactory.init('player', 'emil-app');
      storage.setItem('avatar', data);
    }
  }

  void _load([String? base64]) async {
    if (base64 == null && _path != null && !kIsWeb) {
      final file = File(_path!);
      if (await file.exists()) {
        _image = Image.file(file).image;
        if (notifier != null) {
          notifier!();
        }
      }
    } else if (kIsWeb || base64 != null) {
      late final String? img;

      if (base64 == null) {
        LocalStorage storage = await StorageFactory.init('player', 'emil-app');
        img = storage.getItem('avatar');
      } else {
        img = base64;
      }

      if (img != null) {
        _image = Image.network(img).image;
      }
    }
  }
}

class NotFoundException implements IOException {
  String? filepath;

  NotFoundException([this.filepath]);

  @override
  String toString() =>
      'The file${filepath != null ? ' "$filepath" ' : ' '}was not found.';
}

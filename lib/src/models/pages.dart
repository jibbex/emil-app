class Pages<T> {
  int itemsPerPage;
  late List _items;

  Pages({this.itemsPerPage = 8}) {
    _items = <T>[];
  }

  Pages.fromList(List<T> list,{
    this.itemsPerPage = 8
  }) : _items = list;

  void add(T value) => _items.add(value);
  void addAll(Iterable<T> values) => _items.addAll(values);
  bool remove(T value) => _items.remove(value);
  T removeAt(int index) => _items.removeAt(index);

  List<T> operator [](int index) =>
      _items.sublist(index, _getLastIndex(index)) as List<T>;

  int _getLastIndex(int index) {
    int lastIndex = index + itemsPerPage;
    return lastIndex < _items.length ? lastIndex : _items.length - 1;
  }

  int get length => (_items.length / itemsPerPage).ceil();
}

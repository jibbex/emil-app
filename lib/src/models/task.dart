import 'dart:convert';
import 'dart:math';
import 'package:emil_app/src/models/difficulty/difficulty_pattern.dart';
import 'package:emil_app/src/models/difficulty/operator.dart';
import 'package:math_expressions/math_expressions.dart';

/// Tasks represents a calculation, that can be constructed from an
/// difficulty pattern. Setting the property value will be also evaluate
/// the result.
class Task {
  late double _result;
  late double _value;
  late String _expression;
  static const _kMaxInt = 1 << 16;

  Task([DifficultyPattern? pattern]) {
    _value = 0;
    _result = 0;

    if (pattern != null) {
      _expression = _generateCalculation(pattern);
    }
  }

  factory Task.fromJson(String json) {
    final map = jsonDecode(json);
    Task task = Task();
    task._result = map['result'];
    task._value = map['value'];
    task._expression = map['expression'];

    return task;
  }

  /// Generates a new seed for the random number generator.
  int _reSeed([int seed = 0]) => Random().nextInt(_kMaxInt - seed);

  /// Returns the value that should be calculated from user
  int get value => _value.toInt();

  /// The evaluated result of the expression
  int get result => _result.toInt();

  /// Returns the maximum result value based on the passed operator.
  int _getMax(DifficultyPattern pattern, Operators op) =>
      op == Operators.divide || op == Operators.multiply
          ? pattern.maximum
          : pattern.maximum ~/ pattern.maxOperands;

  /// Sets the user calculated value.
  set value(int value) => _value = value.toDouble();

  /// Sets the expression of the task and evaluates the result.
  set expression(String exprString) {
    final exprStr = exprString.replaceAll('•', '*').replaceAll(':', '/');
    Expression expr = Parser().parse(exprStr);
    _result = expr.evaluate(EvaluationType.REAL, ContextModel());
    _expression = exprStr;
  }

  /// Generates a Task expression according the passed DifficultyPattern,
  /// evaluates the result and returns the String representation of it.
  String _generateCalculation(DifficultyPattern pattern) {
    late String calculation;
    final rngOp = Random(_reSeed()).nextInt(pattern.operators.length);
    final operator = pattern[rngOp].operator;
    final maximum = _getMax(pattern, operator);
    final a = Random(_reSeed(1 << 8)).nextInt(maximum) + pattern.minimum;
    final b = Random(_reSeed(a)).nextInt(maximum) + pattern.minimum;

    switch (operator) {
      case Operators.add:
        calculation = _buildAddition(a, b);
        break;
      case Operators.subtract:
        calculation = _buildSubtraction(a, b);
        break;
      case Operators.multiply:
        calculation = _buildMultiplication(a, b);
        break;
      case Operators.divide:
        calculation = _generateDivision(a, b, maximum);
    }

    calculation = _eval(calculation, pattern);

    while (_operandsCount(calculation) < pattern.maxOperands) {
      final c = Random(_reSeed(a + b)).nextInt(maximum) + pattern.minimum;
      calculation +=
          ' ${Operator.fromOperators(operator).exprSymbol} $c';

      calculation = _eval(calculation, pattern);
    }

    return calculation;
  }

  /// Parses the passed String calculation, evaluates the parsed expression
  /// and sets _result property to its result. Returns either the passed
  /// calculation String or the modified calculation String.
  String _eval(String calculation, DifficultyPattern pattern) {
    final Parser parser = Parser();

    _result = parser.parse(calculation).evaluate(
      EvaluationType.REAL,
      ContextModel(),
    );

    while (!pattern.hasNegativeResult && _result < 0) {
      calculation = calculation.replaceFirst('-', '+');
      _result = parser.parse(calculation).evaluate(
        EvaluationType.REAL,
        ContextModel(),
      );
    }

    return calculation;
  }

  /// Generates an division Task expression from the two passed number values
  /// and a result within the range from 1 to passed `maximum` value and returns
  /// the String representation of it.
  String _generateDivision(num a, num b, int maximum) {
    while(true) {
      var dividend = max(a, b);
      final divisor = min(a, b);
      final remainder = dividend % divisor as int;

      if (remainder > 0) {
        dividend -= remainder;
      }

      final product = dividend * divisor;

      if (product / divisor <= maximum) {
        return _buildDivision(product, divisor);
      }

      a = Random(_reSeed() - product as int).nextInt(a as int) + 1;
      b = Random(_reSeed() + product as int).nextInt(b as int) + 1;
    }
  }

  /// Composes a String of the addition from the two passed number
  /// values and returns it.
  String _buildAddition(num a, num b) => '$a + $b';

  /// Composes a String of the subtraction from the two passed number
  /// values and returns it.
  String _buildSubtraction(num a, num b) => '$a - $b';

  /// Composes a String of the multiplication from the two passed number
  /// values and returns it.
  String _buildMultiplication(num a, num b) => '$a * $b';

  /// Composes a String of the division from the two passed number
  /// values and returns it.
  String _buildDivision(num a, num b) => '$a / $b';

  /// Returns the count of operands of the passed calculation String.
  int _operandsCount(String calculation) =>
      calculation.split(RegExp(r'\+|-|\/|\*')).length;

  /// Returns an JSON encoded String
  String toJson() => jsonEncode({
    'expression': _expression,
    'result': result,
    'value': value,
  });

  /// Returns the properly formatted expression.
  @override
  String toString() => _expression.replaceAll('*', '•').replaceAll('/', ':');
}

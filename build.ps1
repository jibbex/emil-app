$targets = @('web --pwa-strategy=offline-first --web-renderer canvaskit', 'apk', 'windows')

if ((Get-Process "docker" -ErrorAction SilentlyContinue | Select-Object -Property ProcessName).ProcessName -ne "docker") {
    Start-Process -FilePath "C:\Program Files\Docker\Docker\Docker Desktop.exe"
    Write-Host "Starte Docker..."

    while ((Get-Process "docker" -ErrorAction SilentlyContinue | Select-Object -Property ProcessName).ProcessName -ne "docker") {
        Start-Sleep -Second 1
    }
}

perl -i -pe 's/^(version:\s+\d+\.\d+\.\d+\+)(\d+)$/$1.($2+1)/e' pubspec.yaml
git add .\pubspec.yaml
git commit -m "Version bump"

foreach ($target in $targets) {
    cmd /c "flutter build $target $($args[0])"
}

Remove-Item -Path ..\emil-server\src\main\resources\META-INF\resources -Recurse
Copy-Item -Path .\build\web\* -Destination ..\emil-server\src\main\resources\META-INF\resources -Recurse
Copy-Item -Path .\assets\wave.svg -Destination ..\emil-server\src\main\resources\META-INF\resources\wave.svg

cd ..\emil-server
.\mvnw clean
.\mvnw compile
.\build linux

cmd /c 'set JAVA_HOME=C:\graalvm-ce-java11-22.3.2&&"C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvars64.bat"&&.\native-build.bat linux'